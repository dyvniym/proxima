#include <proxima/compose.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/for_each.hpp>
#include <proxima/transducer/pipe.hpp>

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>
#include <string>
#include <vector>

template <typename Integral>
auto
    make_random_vector
    (
        std::size_t size,
        Integral min = std::numeric_limits<Integral>::min(),
        Integral max = std::numeric_limits<Integral>::max(),
        bool seed = false,
        bool sort = false,
        bool descending = false
    )
{
    auto seed_value = seed
        ? static_cast<std::default_random_engine::result_type>
        (
            std::chrono::system_clock::now().time_since_epoch().count()
        )
        : 0;
    std::default_random_engine engine(seed_value);
    std::uniform_int_distribution<Integral> uniform(min, max);

    std::vector<Integral> v(size);
    std::generate(v.begin(), v.end(), [&] () { return uniform(engine); });
    if (sort)
    {
        if (descending)
        {
            std::sort(v.begin(), v.end(), std::greater<>{});
        }
        else
        {
            std::sort(v.begin(), v.end());
        }
    }

    return v;
}

template <typename Process, typename Range>
void
    test
    (
        const std::string & name,
        Process process,
        const Range & times
    )
{
    using namespace std::chrono;
    using std::begin;
    using std::end;

    auto start_time = steady_clock::now();
    auto s = process(begin(times), end(times));
    auto total_time = steady_clock::now() - start_time;

    std::clog << "result = " << s << "\n";

    std::cout << name << ' ' << duration_cast<duration<double>>(total_time).count() << std::endl;
}

auto hard_work (std::int32_t time_to_sleep)
{
    std::this_thread::sleep_for(std::chrono::microseconds(time_to_sleep));
}

template <typename T>
std::ostream & print (std::ostream & s, const std::vector<T> & v)
{
    s << "[";
    for (const auto & e: v)
    {
        s << e << ',';
    }
    return s << "]";
}

const auto proxima_crunch_parallel =
    [] (auto b, auto e)
    {
        return
            proxima::reduce(b, e,
                proxima::compose
                (
                    proxima::for_each(hard_work),
                    proxima::pipe,
                    proxima::for_each(hard_work),
                    proxima::pipe,
                    proxima::for_each(hard_work),
                    proxima::sum
                ));
    };

const auto proxima_crunch =
    [] (auto b, auto e)
    {
        return
            proxima::reduce(b, e,
                proxima::compose
                (
                    proxima::for_each(hard_work),
                    proxima::for_each(hard_work),
                    proxima::for_each(hard_work),
                    proxima::sum
                ));
    };

const auto loop_crunch =
    [] (auto b, auto e)
    {
        auto sum = typename decltype(b)::value_type{0};
        while (b != e)
        {
            hard_work(*b);
            hard_work(*b);
            hard_work(*b);

            sum += *b;
            ++b;
        }

        return sum;
    };

int main (int argc, const char * argv[])
{
    if (argc == 1 + 3)
    {
        const auto size = std::stoul(argv[1]);
        const auto min_time = std::stoi(argv[2]);
        const auto max_time = std::stoi(argv[3]);
        const auto times = make_random_vector<std::int32_t>(size, min_time, max_time);

        test("proxima_parallel", proxima_crunch_parallel, times);
        test("proxima", proxima_crunch, times);
        test("loop", loop_crunch, times);
    }
    else
    {
        const auto usage =
            std::string(argv[0]) +
            " <размер диапазона> "
            "<минимальное ожидание (мкс)> "
            "<максимальное ожидание (мкс)>";
        std::cerr << "Использование: " << usage << std::endl;
        return 1;
    }
}
