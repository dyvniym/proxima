Проксима
========

Простая и эффективная компоновка алгоритмов с поддержкой многопоточности.

[Попробовать онлайн на Wandbox.org](https://wandbox.org/permlink/qZAeI7DpiZsW5vLl)

Содержание
==========

1. [Быстрый старт](#быстрый-старт)
    1. [Простота](#простота)
    2. [Многопоточность](#многопоточность)
    3. [Работает на этапе компиляции](#работает-на-этапе-компиляции)
2. [Справочник](#справочник)
    1. [Преобразователи](#преобразователи)
    2. [Ядра свёртки](#ядра-свёртки)
3. [Документация](#документация)
4. [Установка](#установка)
    1. [Скопировать исходники](#скопировать-исходники)
    2. [Установить с помощью CMake](#установить-с-помощью-cmake)
    3. [Подключить папку с проектом в CMake](#подключить-папку-с-проектом-в-cmake)
5. [Требования](#требования)

Быстрый старт
=============

В духе языка C++, Проксима ориентирована на максимальную абстрактность и простоту в использовании без потери производительности. Приведённые ниже примеры демонстрируют простейшие способы работы с библиотекой.

Простота
--------

```cpp
#include <proxima/compose.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/transform.hpp>
```

```cpp
const int items[] = {1, 2, 3};                           // Исходная последовательность.
const auto kernel =
    proxima::compose                                     // Создаём обработчик, который:
    (
        proxima::transform([] (auto x) {return x * x;}), // 1.  Возведёт в квадрат каждый входной
                                                         //     элемент;
        proxima::sum                                     // 2.  Просуммирует возведённые в квадрат
                                                         //     элементы.
    );
const auto x = proxima::reduce(items, kernel);           // Применение созданного обработчика к
                                                         // исходной последовательности.
assert(x == 14); // 1 * 1 + 2 * 2 + 3 * 3
```

Многопоточность
---------------

Допустим, дан набор путей к файлам в файловой системе, и требуется прочитать эти файлы, разобрать их и привести к некоему общему формату. При этом, естественно, возникает желание раскидать чтение, разбор и нормализацию по разным потокам, чтобы ускорить обработку.

Для этой цели в Проксиме предусмотрен преобразователь-метка `pipe`:

```cpp
#include <proxima/compose.hpp>
#include <proxima/kernel/to_vector.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/pipe.hpp>
#include <proxima/transducer/transform.hpp>
```

```cpp
const auto paths_to_files = ...;
const auto kernel =
    proxima::compose
    (
        proxima::transform(read_file),                  // Первый поток
        proxima::pipe,                      // <-
        proxima::transform(parse_file),                 // Второй поток
        proxima::pipe,                      // <-
        proxima::transform(normalize_file),             // Третий
        proxima::to_vector                              // поток
    );
const auto normalized_files = proxima::reduce(paths_to_files, kernel);
```

Такая запись означает, что в обработке будут участвовать три потока, первый из которых будет читать файл с диска, второй будет производить разбор считанного файла, а третий — нормализовывать формат и сохранять все файлы в `std::vector`.

Работает на этапе компиляции
----------------------------

Просто подставь `constexpr`!

```cpp
#include <proxima/compose.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/stride.hpp>
#include <proxima/transducer/take_while.hpp>
#include <proxima/transducer/transform.hpp>
```

```cpp
constexpr int items[] = {1, 2, 3, 4, 5};
constexpr auto kernel =
    proxima::compose
    (
        proxima::transform([] (auto x) {return x * x;}),   // 1. Каждый элемент возведён в квадрат;
        proxima::stride(2),                                // 2. Берутся только элементы с номерами,
                                                           //    кратными двойке (нумерация с нуля);
        proxima::take_while([] (auto x) {return x < 10;}), // 3. Элементы берутся до тех пор, пока
                                                           //    они меньше десяти;
        proxima::fold(0, std::plus<>{})                    // 4. Результат суммируется.
    );
constexpr auto x = proxima::reduce(std::begin(items), std::end(items), kernel);
static_assert(x == 10); // 1 * 1 + 3 * 3
```

`constexpr` работает не со всеми, но со многими элементами Проксимы.

Справочник
==========

Преобразователи
---------------

В таблице ниже приняты следующие обозначения:
1.  `n` обозначает значение любого целочисленного типа;
2.  `p` обозначает одноместный предикат;
3.  `f` обозначает одноместную функцию без возвращаемого результата (на самом деле возвращаемый результат может и быть, но будет проигнорирован);
4.  `e` обозначает двухместный предикат, смысл которого — определение равенства двух элементов;
5.  `g` обозначает одноместную функцию с возвращаемым результатом;
6.  `s` обозначает двухместную функцию, оба аргумента и возвращамое значение которой — это значение того же типа;
7.  `x` обозначает любое значение;
8.  `h` обозначает двухместную функцию, первый аргумент и возвращаемое значение которой — это значение того же типа, что и `x`, а второй аргумент — произвольное значение;
9.  Квадратные скобки `[]` обозначают необязательность того или иного параметра.

| Преобразователь                                                         | Описание                                                                                                                                        | Заголовок                             |
|:------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------|
| [`chunk(n)`](include/proxima/transducer/chunk.hpp)                      | Разбить входную последовательность на куски размера `n`                                                                                         | proxima/transducer/chunk.hpp          |
| [`drop(n)`](include/proxima/transducer/drop.hpp)                        | Проигнорировать первые `n` элементов последовательности                                                                                         | proxima/transducer/drop.hpp           |
| [`drop_if(p)`](include/proxima/transducer/drop_if.hpp)                  | Проигнорировать входящие элементы, удовлетворяющие предикату `p`                                                                                | proxima/transducer/drop_if.hpp        |
| [`drop_while(p)`](include/proxima/transducer/drop_while.hpp)            | Игнорировать входящие элементы до тех пор, пока они удовлетворяют предикату `p`                                                                 | proxima/transducer/drop_while.hpp     |
| [`enumerate[(n)]`](include/proxima/transducer/enumerate.hpp)            | Занумеровать входящие элементы (если указано, то начать нумерацию с `n`)                                                                        | proxima/transducer/enumerate.hpp      |
| [`exclusive_scan(x, h)`](include/proxima/transducer/exclusive_scan.hpp) | Заменить каждый элемент суммой (суммирование задаёт функция `h`) начального значения `x` и всех предыдущих элементов, исключая сам этот элемент | proxima/transducer/exclusive_scan.hpp |
| [`for_each(f)`](include/proxima/transducer/for_each.hpp)                | Произвести операцию `f` над каждым входным элементом                                                                                            | proxima/transducer/for_each.hpp       |
| [`group_by(e)`](include/proxima/transducer/group_by.hpp)                | Сгруппировать элементы, которые равны относительно предиката `e`                                                                                | proxima/transducer/group_by.hpp       |
| [`inclusive_scan(x, h)`](include/proxima/transducer/inclusive_scan.hpp) | Заменить каждый элемент суммой (суммирование задаёт функция `h`) начального значения `x` и всех предыдущих элементов, включая сам этот элемент  | proxima/transducer/inclusive_scan.hpp |
| [`join`](include/proxima/transducer/join.hpp)                           | Превратить диапазон диапазонов в плоский диапазон                                                                                               | proxima/transducer/join.hpp           |
| [`pairwise`](include/proxima/transducer/pairwise.hpp)                   | Пройтись попарно по элементам входной последовательности                                                                                        | proxima/transducer/pairwise.hpp       |
| [`partial_sum[(s)]`](include/proxima/transducer/partial_sum.hpp)        | Заменить каждый элемент суммой (если указано, использовать в качестве суммы функцию `s`) всех предыдущих элементов, включая сам этот элемент    | proxima/transducer/partial_sum.hpp    |
| [`pipe`](include/proxima/transducer/pipe.hpp)                           | Включить многопоточность                                                                                                                        | proxima/transducer/pipe.hpp           |
| [`stride(n)`](include/proxima/transducer/stride.hpp)                    | Оставить только каждый `n`-й из входящих элементов                                                                                              | proxima/transducer/stride.hpp         |
| [`take(n)`](include/proxima/transducer/take.hpp)                        | Оставить только `n` первых входящих элементов                                                                                                   | proxima/transducer/take.hpp           |
| [`take_if(p)`](include/proxima/transducer/take_if.hpp)                  | Оставить только те из входящих элементов, которые удовлетворяют предикату `p`                                                                   | proxima/transducer/take_if.hpp        |
| [`take_while(p)`](include/proxima/transducer/take_while.hpp)            | Брать входящие элементы до тех пор, пока они удовлетворяют предикату `p`                                                                        | proxima/transducer/take_while.hpp     |
| [`transform(g)`](include/proxima/transducer/transform.hpp)              | Преобразовать входящие элементы с помощью функции `g`                                                                                           | proxima/transducer/transform.hpp      |
| [`unique[(e)]`](include/proxima/transducer/unique.hpp)                  | Оставить только уникальные элементы (если указан предикат `e`, то равенство определяется относительно него)                                     | proxima/transducer/unique.hpp         |
| [`window(n)`](include/proxima/transducer/window.hpp)                    | Пройтись по входной последовательности скользящим окном размера `n`                                                                             | proxima/transducer/window.hpp         |

Ядра свёртки
------------

В таблице ниже приняты следующие обозначения:
1.  `n` обозначает значение любого целочисленного типа;
2.  `x` обозначает любое значение;
3.  `f` обозначает двухместную функцию, первый аргумент и возвращаемое значение которой — это значение того же типа, что и `x`, а второй аргумент — произвольное значение;
4.  `i` обозначает выходной итератор;
5.  Квадратные скобки `[]` обозначают необязательность того или иного параметра.

| Ядро                                                              | Описание                                                                                                  | Заголовок                    |
|:----------------------------------------------------------------- |:--------------------------------------------------------------------------------------------------------- |:---------------------------- |
| [`count[(n)]`](include/proxima/kernel/count.hpp)                  | Подсчитать входящие символы (если указано, то начать отсчёт с `n`)                                        | proxima/kernel/count.hpp     |
| [`fold(x, f)`](include/proxima/kernel/fold.hpp)                   | Применить к последовательности пользовательскую свёртку `f` с начальным значением `x`                     | proxima/kernel/fold.hpp      |
| [`output(i)`](include/proxima/kernel/output.hpp)                  | Записать входящие элементы в выходной итератор `i`                                                        | proxima/kernel/output.hpp    |
| [`product`](include/proxima/kernel/product.hpp)                   | Перемножить входящие символы                                                                              | proxima/kernel/product.hpp   |
| [`sum`](include/proxima/kernel/sum.hpp)                           | Просуммировать входящие символы                                                                           | proxima/kernel/sum.hpp       |
| [`to_vector[(reserve(n))]`](include/proxima/kernel/to_vector.hpp) | Сохранить входящие символы в `std::vector` (если указано, то заранее выделяется память под `n` элементов) | proxima/kernel/to_vector.hpp |

Документация
============

[Описание модели и терминология](doc/README.md).

Установка
=========

Возможны следующие четыре варианта установки.

Вариант 1: CMake FetchContent
-----------------------------

Начиная с версии CMake 3.14 можно скачать и подключить репозиторий с зависимостью прямо во время сборки с помощью модуля [FetchContent](https://cmake.org/cmake/help/v3.14/module/FetchContent.html). В случае с Проксимой это можно записать тремя командами:

```cmake
include(FetchContent)
FetchContent_Declare(Proxima GIT_REPOSITORY https://gitlab.com/star-system/proxima.git)
FetchContent_MakeAvailable(Proxima)
```

Этот набор команд породит интерфейсную библиотеку `Proxima::proxima`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE Proxima::proxima)
```

Вариант 2: Установить с помощью CMake
-------------------------------------

```shell
cd path/to/build/directory
cmake -DCMAKE_BUILD_TYPE=Release path/to/proxima
make
make install
```

После этого в системе сборки CMake будет доступен пакет `Proxima`:

```cmake
find_package(Proxima)
```

Эта команда породит интерфейсную библиотеку `Proxima::proxima`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE Proxima::proxima)
```

Вариант 3: Скопировать исходники
--------------------------------

Поскольку Проксима — полностью заголовочная библиотека, то достаточно скопировать в нужную директорию все заголовки из папки `include` из [репозитория](https://gitlab.com/star-system/proxima) и подключить их в свой проект.

Вариант 4: Подключить папку с проектом в CMake
----------------------------------------------

```cmake
add_subdirectory("path/to/proxima")
```

После этого в системе сборки CMake будет доступна цель `Proxima::proxima`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE Proxima::proxima)
```

Требования
==========

1.  Любой компилятор, который поддерживает стандарт C++17 не хуже, чем GCC 7.3 или Clang 6.0;
2.  Библиотека тестирования [Catch2](https://github.com/catchorg/Catch2) [Не обязательно\*];
3.  [Doxygen](http://doxygen.nl) [Не обязательно].

> \*) Можно миновать этап сборки и тестирования, если при сборке с помощью `CMake` выключить опцию `PROXIMA_TESTING`:
>
> ```shell
> cmake -DCMAKE_BUILD_TYPE=Release path/to/proxima -DPROXIMA_TESTING=OFF
> ```
>
> Также тестирование автоматически отключается в случае, если Проксима подключается в качестве подпроекта.
