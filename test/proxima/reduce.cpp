#include <proxima/compose.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/kernel/output.hpp>
#include <proxima/kernel/to_vector.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/for_each.hpp>
#include <proxima/transducer/group_by.hpp>
#include <proxima/transducer/take.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <functional>
#include <vector>

TEST_CASE("С простым ядром свёртки \"reduce\" проходит по всем символам из диапазона", "[reduce]")
{
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto output = proxima::output(values.begin());

    auto composite = proxima::compose(for_each, output);
    auto s = proxima::initialize(composite, proxima::type<int>);

    proxima::reduce(values.begin(), values.end(), s, composite);

    REQUIRE(std::all_of(values.begin(), values.end(), [] (auto x) {return x == 0;}));
}

TEST_CASE("Финальное состояние достигается посредством вызова \"reduce\"", "[reduce]")
{
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto take = proxima::take(3);
    auto output = proxima::output(values.begin());

    auto composite = proxima::compose(for_each, take, output);
    auto not_final = proxima::initialize(composite, proxima::type<int>);
    REQUIRE(not proxima::is_final(composite, not_final));

    auto final_expected =
        proxima::reduce(values.begin(), values.end(), std::move(not_final), composite);
    REQUIRE(proxima::is_final(composite, final_expected));
}

TEST_CASE("Ровно столько элементов диапазона окажутся затронутыми, "
    "сколько шагов произойдёт в \"reduce\" до перехода в финальное состояние", "[reduce]")
{
    constexpr auto steps_till_finalization = 3;
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto take = proxima::take(steps_till_finalization);
    auto output = proxima::output(values.begin());

    auto composite = proxima::compose(for_each, take, output);
    auto s = proxima::initialize(composite, proxima::type<int>);

    proxima::reduce(values.begin(), values.end(), std::move(s), composite);

    REQUIRE(std::all_of
    (
        values.begin(),
        values.begin() + steps_till_finalization,
        [] (auto x) {return x == 0;}
    ));
    REQUIRE(std::all_of
    (
        values.begin() + steps_till_finalization,
        values.end(),
        [] (auto x) {return x == 1;}
    ));
}

TEST_CASE("После перехода в финальное состояние \"reduce\" не меняет значения состояния", "[reduce]")
{
    constexpr auto steps_till_finalization = 3;
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto take = proxima::take(steps_till_finalization);
    auto output = proxima::output(values.begin());

    auto composite = proxima::compose(for_each, take, output);
    auto s = proxima::initialize(composite, proxima::type<int>);

    auto final =
        proxima::reduce
        (
            values.begin(),
            values.begin() + steps_till_finalization,
            std::move(s),
            composite
        );

    auto after_final =
        proxima::reduce
        (
            values.begin() + steps_till_finalization,
            values.begin() + steps_till_finalization + 1,
            final,
            composite
        );

    REQUIRE(proxima::is_final(composite, after_final));
    REQUIRE(proxima::value(final) == proxima::value(after_final));
}

TEST_CASE("С ядром, допускающим завершение по требованию, перегрузка \"reduce\", принимающая "
    "состояние, не высвобождает значение состояния", "[reduce]")
{
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto to_vector = proxima::to_vector;

    auto composite = proxima::compose(for_each, group_by, to_vector);
    auto s = proxima::initialize(composite, proxima::type<int>);
    auto new_s = proxima::reduce(values.begin(), values.end(), std::move(s), composite);
    REQUIRE(proxima::value(new_s).empty());
    SECTION("и при использовании этой перегрузки"
        " высвобождение произойдёт только после явного вызова \"emit\"")
    {
        proxima::emit(composite, new_s);
        REQUIRE(not proxima::value(new_s).empty());
        auto result = std::move(proxima::value(new_s));
        REQUIRE(std::all_of
        (
            result.front().begin(),
            result.front().end(),
            [] (auto x) {return x == 0;}
        ));
    }
}

TEST_CASE("С ядром, допускающим завершение по требованию, перегрузка \"reduce\", не принимающая "
    "состояния, высвобождает значение состояния", "[reduce]")
{
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto for_each = proxima::for_each([] (auto & x) {--x;});
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto to_vector = proxima::to_vector;

    auto composite = proxima::compose(for_each, group_by, to_vector);
    auto result = proxima::reduce(values.begin(), values.end(), composite);
    REQUIRE(not result.empty());
    REQUIRE(std::all_of(result.front().begin(), result.front().end(),
        [] (auto x) {return x == 0;}));
}

TEST_CASE("Функция \"reduce\", имеет перегрузку для диапазона для любого типа ядра ", "[reduce]")
{
    std::vector<int> values = {1, 1, 1, 1, 1};

    auto group_by = proxima::group_by(std::equal_to<>{});
    auto take = proxima::take(3);

    auto to_vector = proxima::to_vector;

    auto simple_kernel = proxima::compose(to_vector);
    auto finalizable_kernel = proxima::compose(take, to_vector);
    auto emmitting_kernel = proxima::compose(group_by, to_vector);
    auto all_in = proxima::compose(take, group_by, to_vector);

    auto r1 = proxima::reduce(values, simple_kernel);
    auto r2 = proxima::reduce(values, finalizable_kernel);
    auto r3 = proxima::reduce(values, emmitting_kernel);
    auto r4 = proxima::reduce(values, all_in);

    REQUIRE(r1.size() == values.size());
    REQUIRE(std::all_of(r1.begin(), r1.end(), [] (auto x) {return x == 1;}));

    REQUIRE(r2.size() == 3ul);
    REQUIRE(std::all_of(r2.begin(), r2.end(), [] (auto x) {return x == 1;}));

    REQUIRE(r3.size() == 1ul);
    REQUIRE(std::all_of(r3.front().begin(), r3.front().end(), [] (auto x) {return x == 1;}));

    REQUIRE(r4.size() == 1ul);
    REQUIRE(r4.front().size() == 3ul);
    REQUIRE(std::all_of(r4.front().begin(), r4.front().end(), [] (auto x) {return x == 1;}));
}

namespace // anonymous
{
    template <typename Iterator>
    struct end_t {Iterator i;};

    template <typename Iterator>
    auto make_end (Iterator i) -> end_t<Iterator>
    {
        return {i};
    }

    template <typename T>
    struct range_t
    {
        auto begin () const
        {
            return values.begin();
        }

        auto end () const
        {
            return make_end(values.end());
        }

        auto begin ()
        {
            return values.begin();
        }

        auto end ()
        {
            return make_end(values.end());
        }

        std::vector<T> values;
    };

    template <typename Iterator>
    bool operator == (Iterator i, end_t<Iterator> s)
    {
        return i == s.i;
    }

    template <typename Iterator>
    bool operator != (Iterator i, end_t<Iterator> s)
    {
        return i != s.i;
    }

    template <typename Iterator>
    bool operator == (end_t<Iterator> s, Iterator i)
    {
        return s.i == i;
    }

    template <typename Iterator>
    bool operator != (end_t<Iterator> s, Iterator i)
    {
        return s.i != i;
    }
} // namespace anonymous

TEST_CASE("Функция \"reduce\", поддерживает диапазоны с ограничителями", "[reduce]")
{
    auto range = range_t<int>{std::vector<int>{1, 2, 3, 4, 5, 6}};
    auto to_vector = proxima::to_vector;

    auto result = proxima::reduce(range.begin(), range.end(), to_vector);
    REQUIRE(std::equal(result.begin(), result.end(), range.begin()));

    auto s = proxima::reduce(range, proxima::initialize(to_vector, proxima::type<int>), to_vector);
    REQUIRE(std::equal(proxima::value(s).begin(), proxima::value(s).end(), range.begin()));

    auto values = proxima::reduce(range, to_vector);
    REQUIRE(std::equal(values.begin(), values.end(), range.begin()));
}

TEST_CASE("Функция \"reduce\" может работать с ядром свёртки в качестве временного объекта",
    "[reduce]")
{
    const auto v = std::vector{1, 2, 3, 4};
    const auto sum = proxima::reduce(v, proxima::fold(0, std::plus<>{}));
    CHECK(sum == 10);
}

TEST_CASE("Функция \"reduce\" умеет принимать первым аргументом временный контейнер", "[reduce]")
{
    const auto sum = proxima::reduce(std::vector{1, 2, 3, 4}, proxima::fold(0, std::plus<>{}));
    CHECK(sum == 10);
}

TEST_CASE("Функция \"reduce\" умеет принимать список инициализации в качестве диапазона",
    "[reduce]")
{
    const auto product = proxima::reduce({1, 2, 3, 4}, proxima::fold(1, std::multiplies<>{}));
    CHECK(product == 24);

    const auto k = proxima::fold(0, std::plus<>{});
    const auto s0 = proxima::initialize(k, proxima::type<int>);
    const auto s1 = proxima::reduce({5, 6, 7, 8}, s0, k);
    CHECK(value(s1) == 26);
}
