#include <proxima/kernel/product.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

TEST_CASE("Ядро \"product\" вычисляет произведение элементов, считанных со входной ленты",
    "[product][kernel]")
{
    const auto k = proxima::product;
    auto s = proxima::initialize(k, proxima::type<int>);

    k(2, s);
    k(3, s);
    k(4, s);

    REQUIRE(value(s) == 2 * 3 * 4);
}

TEST_CASE("Тип значения состояния ядра \"product\" в точности равен типу символов на входной ленте",
    "[product][kernel]")
{
    const auto k = proxima::product;
    auto s = proxima::initialize(k, proxima::type<double>);

    k(int{2}, s);
    k(int{3}, s);
    k(int{4}, s);

    REQUIRE(std::is_same_v<proxima::state_value_t<decltype(s)>, double>);
}

TEST_CASE("Ядро \"product\" может быть вычислено на этапе компиляции", "[product][kernel]")
{
    constexpr auto k = proxima::product;
    constexpr auto s0 = proxima::initialize(k, proxima::type<int>);

    constexpr auto s1 = proxima::utility::invoke(k, 5, s0);
    constexpr auto s2 = proxima::utility::invoke(k, 6, s1);
    constexpr auto s3 = proxima::utility::invoke(k, 7, s2);

    static_assert(value(s3) == 5 * 6 * 7);
    REQUIRE(value(s3) == 5 * 6 * 7);
}
