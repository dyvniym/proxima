#include <proxima/kernel/count.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <cstddef>
#include <type_traits>

TEST_CASE("Ядро \"count\" вычисляет количество элементов, считанных со входной ленты",
    "[count][kernel]")
{
    const auto k = proxima::count;
    auto s = proxima::initialize(k, proxima::type<int>);

    const auto elements = {1, 2, 3};

    for (auto x: elements)
    {
        k(x, s);
    }

    REQUIRE(value(s) == elements.size());
}

TEST_CASE("Тип значения состояния ядра \"count\" (то есть счётчика) по-умолчанию — это std::size_t",
    "[count][kernel]")
{
    const auto k = proxima::count;
    auto s = proxima::initialize(k, proxima::type<double>);

    k(int{1}, s);
    k(int{2}, s);
    k(int{3}, s);

    REQUIRE(std::is_same_v<proxima::state_value_t<decltype(s)>, std::size_t>);
}

TEST_CASE("Тип счётчика может определяться начальным значением", "[count][kernel]")
{
    const auto k = proxima::count(char{5});
    auto s = proxima::initialize(k, proxima::type<double>);

    k(int{1}, s);
    k(int{2}, s);
    k(int{3}, s);

    REQUIRE(std::is_same_v<proxima::state_value_t<decltype(s)>, char>);
}

TEST_CASE("Итоговый результат свёртки с ядром \"count\" равен сумме начального значения и "
    "количества элементов, считанных со входной ленты", "[count][kernel]")
{
    const auto k = proxima::count(short{5});
    auto s = proxima::initialize(k, proxima::type<std::size_t>);

    k(1, s);
    k(2, s);
    k(3, s);

    REQUIRE(value(s) == 5 + 3);
}

TEST_CASE("Свёртка с ядром \"count\" может быть вычислена на этапе компиляции", "[count][kernel]")
{
    constexpr auto k = proxima::count;
    constexpr auto s0 = proxima::initialize(k, proxima::type<int>);

    constexpr auto s1 = proxima::utility::invoke(k, 100, s0);
    constexpr auto s2 = proxima::utility::invoke(k, 200, s1);
    constexpr auto s3 = proxima::utility::invoke(k, 300, s2);

    static_assert(value(s3) == 3);
    REQUIRE(value(s3) == 3);
}
