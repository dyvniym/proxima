#include <proxima/kernel/sum.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

TEST_CASE("Ядро \"sum\" вычисляет сумму элементов, считанных со входной ленты", "[sum][kernel]")
{
    const auto k = proxima::sum;
    auto s = proxima::initialize(k, proxima::type<int>);

    k(1, s);
    k(2, s);
    k(3, s);

    REQUIRE(value(s) == 1 + 2 + 3);
}

TEST_CASE("Тип значения состояния ядра \"sum\" в точности равен типу символов на входной ленте",
    "[sum][kernel]")
{
    const auto k = proxima::sum;
    auto s = proxima::initialize(k, proxima::type<double>);

    k(int{1}, s);
    k(int{2}, s);
    k(int{3}, s);

    REQUIRE(std::is_same_v<proxima::state_value_t<decltype(s)>, double>);
}

TEST_CASE("Ядро \"sum\" может быть вычислено на этапе компиляции", "[sum][kernel]")
{
    constexpr auto k = proxima::sum;
    constexpr auto s0 = proxima::initialize(k, proxima::type<int>);

    constexpr auto s1 = proxima::utility::invoke(k, 100, s0);
    constexpr auto s2 = proxima::utility::invoke(k, 200, s1);
    constexpr auto s3 = proxima::utility::invoke(k, 300, s2);

    static_assert(value(s3) == 100 + 200 + 300);
    REQUIRE(value(s3) == 100 + 200 + 300);
}
