#include <proxima/kernel/fold.hpp>
#include <proxima/state/simple_state.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

TEST_CASE("Ядро \"fold_t\" вычисляет на этапе компиляции", "[fold]")
{
    constexpr auto init = 0;
    constexpr auto plus = [] (auto x, auto y) constexpr {return x + y;};

    constexpr auto k = proxima::fold(init, plus);
    constexpr auto s = proxima::initialize(k, proxima::type<int>);

    constexpr auto new_state = proxima::utility::invoke(k, 1, s);
    static_assert(proxima::value(new_state) == 1);
    REQUIRE(proxima::value(new_state) == 1);
}

TEST_CASE("Ядро \"fold_t\" изменяет состояние, применяя заданную функцию", "[fold]")
{
    const auto init = 0;
    const auto plus = [] (auto x, auto y) {return x + y;};

    const auto k = proxima::fold(init, plus);
    const auto s = proxima::initialize(k, proxima::type<int>);

    const auto new_state = proxima::utility::invoke(k, 1, s);
    REQUIRE(proxima::value(new_state) == 1);
}
