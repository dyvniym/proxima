#include <proxima/kernel/to_vector.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

TEST_CASE("Новосозданное состояние ядра свёртки \"to_vector_t\" — пустой массив",
    "[kernel][to_vector]")
{
    const auto kernel = proxima::to_vector;
    const auto state = proxima::initialize(kernel, proxima::type<int>);

    REQUIRE(proxima::value(state).empty());
}

TEST_CASE("Ядро \"to_vector_t\" дописывает входной символ в конец массива", "[kernel][to_vector]")
{
    const auto kernel = proxima::to_vector;
    const auto symbol = 123;
    const auto state = proxima::initialize(kernel, proxima::type<int>);

    const auto new_state = proxima::utility::invoke(kernel, symbol, state);
    REQUIRE(proxima::value(new_state).back() == symbol);
}

TEST_CASE("Ядро \"to_vector_t\" допускает заблаговременное резервирование памяти",
    "[kernel][to_vector]")
{
    const auto kernel = proxima::to_vector(proxima::reserve(100));
    const auto state = proxima::initialize(kernel, proxima::type<long>);
    REQUIRE(proxima::value(state).capacity() >= 100);
}
