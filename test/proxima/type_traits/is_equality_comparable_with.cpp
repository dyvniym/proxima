#include <proxima/type_traits/is_equality_comparable_with.hpp>

#include <catch2/catch.hpp>

#include <cstdint>
#include <optional>

TEST_CASE("Целочисленные типы сравнимы сами с собой", "[is_equality_comparable_with]")
{
    static_assert(proxima::is_equality_comparable_with_v<int, int>);
    REQUIRE(proxima::is_equality_comparable_with_v<int, int>);

    static_assert(proxima::is_equality_comparable_with_v<char, char>);
    REQUIRE(proxima::is_equality_comparable_with_v<char, char>);

    static_assert(proxima::is_equality_comparable_with_v<std::uint16_t, std::uint16_t>);
    REQUIRE(proxima::is_equality_comparable_with_v<std::uint16_t, std::uint16_t>);

    static_assert(proxima::is_equality_comparable_with_v<std::int64_t, std::int64_t>);
    REQUIRE(proxima::is_equality_comparable_with_v<std::int64_t, std::int64_t>);
}

TEST_CASE("Сравнимость симетрична", "[is_equality_comparable_with]")
{
    static_assert(proxima::is_equality_comparable_with_v<int, std::int16_t>);
    REQUIRE(proxima::is_equality_comparable_with_v<int, std::int16_t>);
    static_assert(proxima::is_equality_comparable_with_v<std::int16_t, int>);
    REQUIRE(proxima::is_equality_comparable_with_v<std::int16_t, int>);
}

namespace // anonymous
{
    struct not_fully_equality_comparable_t {};

    [[maybe_unused]]
    bool operator == (const not_fully_equality_comparable_t &, const int &)
    {
        return false;
    }

    [[maybe_unused]]
    bool operator != (const not_fully_equality_comparable_t &, const int &)
    {
        return true;
    }

} // namespace anonymous

TEST_CASE("Типы не сравнимы, если сравнение не симетрично", "[is_equality_comparable_with]")
{
    static_assert(not proxima::is_equality_comparable_with_v<not_fully_equality_comparable_t, int>);
    REQUIRE_FALSE(proxima::is_equality_comparable_with_v<not_fully_equality_comparable_t, int>);
}

namespace // anonymous
{
    struct equality_comparable_t {};

    std::optional<int> operator == (const equality_comparable_t &, const int &)
    {
        return 1;
    }

    std::optional<int> operator != (const equality_comparable_t &, const int &)
    {
        return 2;
    }

    std::optional<int> operator == (const int &, const equality_comparable_t &)
    {
        return 3;
    }

    std::optional<int> operator != (const int &, const equality_comparable_t &)
    {
        return 4;
    }

} // namespace anonymous

TEST_CASE("При неявном преобразовнии к bool результата сравнения "
    " типы считаются сравнимыми", "[is_equality_comparable_with]")
{
    static_assert(proxima::is_equality_comparable_with_v<equality_comparable_t, int>);
    REQUIRE(proxima::is_equality_comparable_with_v<equality_comparable_t, int>);
}
