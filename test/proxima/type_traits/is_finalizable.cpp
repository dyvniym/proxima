#include <proxima/state/simple_state.hpp>
#include <proxima/type_traits/is_finalizable.hpp>

#include <catch2/catch.hpp>

namespace // anonymous
{
    struct finalizable_pseudokernel_t {};

    auto initialize (const finalizable_pseudokernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    template <typename V>
    bool is_final (const finalizable_pseudokernel_t &, const proxima::simple_state_t<V> &)
    {
        return true;
    }

    struct nonfinalizable_pseudokernel_t {};
} // namespace anonymous

TEST_CASE("Метафункция \"is_finalizable\"", "[finalizable]")
{
    SECTION("возвращает истину на заведомо финализируемом ядре")
    {
        CHECK(proxima::is_finalizable_v<finalizable_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на заведомо нефинализируемом ядре")
    {
        CHECK_FALSE
        (
            proxima::is_finalizable_v<nonfinalizable_pseudokernel_t, int>
        );
    }
    SECTION("возвращает ложь на ядре с несоответствующим ему типом состояния")
    {
        CHECK_FALSE(proxima::is_finalizable_v<finalizable_pseudokernel_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::is_finalizable_v<double, long>);
    }
}
