#include <proxima/state/simple_state.hpp>
#include <proxima/type_traits/is_reducible.hpp>

#include <catch2/catch.hpp>

namespace // anonymous
{
    struct dummy_reduce_kernel_t
    {
        template <typename Symbol, typename State>
        void operator () (Symbol &&, State &) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }
} // namespace anonymous

namespace // anonymous
{
    struct dummy_too_few_arguments_reduce_kernel_t
    {
        template <typename A>
        void operator () (A &&) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_too_few_arguments_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct dummy_too_many_arguments_reduce_kernel_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_too_many_arguments_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }
}

TEST_CASE("Метафункция \"is_reducible\"", "[reducible]")
{
    SECTION("возвращает истину на ядре свёртки")
    {
        CHECK(proxima::is_reducible_v<dummy_reduce_kernel_t, int>);
    }
    SECTION("возвращает ложь на ядре с несоответствующим символом")
    {
        CHECK_FALSE(proxima::is_reducible_v<dummy_reduce_kernel_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::is_reducible_v<double, int>);
    }
}

TEST_CASE("Метафункция \"is_reducible\" возвращает ложь на ядре с несоответствующей сигнатурой "
    "вызова", "[reducible]")
{
    CHECK_FALSE(proxima::is_reducible_v<dummy_too_few_arguments_reduce_kernel_t, int>);
    CHECK_FALSE(proxima::is_reducible_v<dummy_too_many_arguments_reduce_kernel_t, int>);
}
