#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/is_stateful.hpp>

#include <catch2/catch.hpp>

namespace // anonymous
{
    struct stateful_pseudokernel_t {};

    template <typename V>
    constexpr auto initialize (const stateful_pseudokernel_t &, proxima::type_t<V>)
    {
        return proxima::simple_state_t<V>{V{}};
    }

    struct stateless_pseudokernel_t {};
} // namespace anonymous

TEST_CASE("Метафункция \"is_stateful\"", "[stateful]")
{
    SECTION("возвращает истину на ядре с состоянием")
    {
        CHECK(proxima::is_stateful_v<stateful_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на ядре без состояния")
    {
        CHECK_FALSE(proxima::is_stateful_v<stateless_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::is_stateful_v<double, int>);
    }
}
