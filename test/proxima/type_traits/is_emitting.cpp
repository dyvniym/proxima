#include <proxima/state/simple_state.hpp>
#include <proxima/type_traits/is_emitting.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

namespace // anonymous
{
    struct emitting_kernel_t;

    constexpr auto initialize (const emitting_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct emitting_kernel_t
    {
        void operator () (int, proxima::state_of_t<emitting_kernel_t, int> &) const
        {
        }
    };

    constexpr void emit (const emitting_kernel_t &, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct non_emitting_kernel_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const non_emitting_kernel_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
} // namespace anonymous

namespace // anonymous
{
    struct wrong_kernel_emit_signature_t
    {
        void operator () (int, proxima::simple_state_t<int> &) const
        {
        }
    };

    constexpr auto initialize (const wrong_kernel_emit_signature_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    constexpr void emit (const wrong_kernel_emit_signature_t &, proxima::simple_state_t<double> &)
    {
    }

    struct too_few_kernel_emit_arguments_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_kernel_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    constexpr void emit (const too_few_kernel_emit_arguments_t &)
    {
    }

    struct too_many_kernel_emit_arguments_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_kernel_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A, typename B>
    constexpr void emit (const too_many_kernel_emit_arguments_t &, A &&, B &&)
    {
    }
}

namespace // anonymous
{
    struct neither_kernel_nor_transducer_t
    {
    };

    constexpr auto initialize (const neither_kernel_nor_transducer_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    template <typename T>
    constexpr void
        emit (const neither_kernel_nor_transducer_t &, T &&, proxima::simple_state_t<int> &)
    {
    }

    constexpr void emit (const neither_kernel_nor_transducer_t &, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct too_few_arguments_kernel_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }
    };

    constexpr auto initialize (const too_few_arguments_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    constexpr void emit (const too_few_arguments_kernel_t &, proxima::simple_state_t<int> &)
    {
    }

    struct too_many_arguments_kernel_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_arguments_kernel_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A>
    constexpr void emit (const too_many_arguments_kernel_t &, proxima::simple_state_t<A> &)
    {
    }
}

namespace // anonymous
{
    struct emitting_transducer_t;

    constexpr auto initialize (const emitting_transducer_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct emitting_transducer_t
    {
        template <typename Tape>
        void operator () (int, Tape &&, proxima::simple_state_t<int>) const
        {
        }
    };

    template <typename Tape>
    constexpr void emit (const emitting_transducer_t &, Tape &&, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct non_emitting_transducer_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const non_emitting_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
}

namespace // anonymous
{
    struct wrong_transducer_emit_signature_t
    {
        template <typename T>
        constexpr void operator () (int, T &&, proxima::simple_state_t<int> &) const
        {
        }
    };

    constexpr auto initialize (const wrong_transducer_emit_signature_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    template <typename T>
    constexpr void
        emit (const wrong_transducer_emit_signature_t &, T &&, proxima::simple_state_t<double> &)
    {
    }

    struct too_few_transducer_emit_arguments_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_transducer_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A>
    constexpr void emit (const too_few_transducer_emit_arguments_t &, A &&)
    {
    }

    struct too_many_transducer_emit_arguments_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_transducer_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A, typename B, typename C>
    constexpr void emit (const too_many_transducer_emit_arguments_t &, A &&, B &&, C &&)
    {
    }
} // namespace anonymous

namespace // anonymous
{
    struct too_few_arguments_transducer_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }

        template <typename A, typename B>
        void operator () (A &&, B &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_arguments_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename T, typename A>
    constexpr void emit (const too_few_arguments_transducer_t &, T &&, proxima::simple_state_t<A> &)
    {
    }

    struct too_many_arguments_transducer_t
    {
        template <typename A, typename B, typename C, typename D>
        void operator () (A &&, B &&, C &&, D &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_arguments_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename T, typename A>
    constexpr void
        emit (const too_many_arguments_transducer_t &, T &&, proxima::simple_state_t<A> &)
    {
    }
}

TEST_CASE("Метафункция \"is_emitting\" возвращает истину, если для ядра определена функция emit",
    "[emit][kernel]")
{
    CHECK(proxima::is_emitting<emitting_kernel_t, int>::value);
    CHECK(proxima::is_emitting_v<emitting_kernel_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если для ядра не определена функция emit",
    "[emit][kernel]")
{
    CHECK_FALSE(proxima::is_emitting<non_emitting_kernel_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<non_emitting_kernel_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если состояние для функции emit для ядра "
    "не подходит по сигнатуре", "[emit][kernel]")
{
    CHECK_FALSE(proxima::is_emitting<emitting_kernel_t, double>::value);
    CHECK_FALSE(proxima::is_emitting_v<emitting_kernel_t, double>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если функция emit для ядра имеет "
    "неправильную сигнатуру", "[emit][kernel]")
{
    CHECK_FALSE(proxima::is_emitting<wrong_kernel_emit_signature_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<wrong_kernel_emit_signature_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_few_kernel_emit_arguments_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_few_kernel_emit_arguments_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_many_kernel_emit_arguments_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_many_kernel_emit_arguments_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если ядро свёртки имеет неправильную "
    "сигнатуру вызова", "[emit][kernel]")
{
    CHECK_FALSE(proxima::is_emitting<too_few_arguments_kernel_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_few_arguments_kernel_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_many_arguments_kernel_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_many_arguments_kernel_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если функция emit определена для объекта, "
    "не являющегося ни преобразователем, ни ядром свёртки", "[emit][kernel][transducer]")
{
    CHECK_FALSE(proxima::is_emitting<neither_kernel_nor_transducer_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<neither_kernel_nor_transducer_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает истину, если для преобразователя определена "
    "функция emit", "[emit][transducer]")
{
    CHECK(proxima::is_emitting<emitting_transducer_t, int>::value);
    CHECK(proxima::is_emitting_v<emitting_transducer_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если для преобразователя не определена "
    "функция emit", "[emit][transducer]")
{
    CHECK_FALSE(proxima::is_emitting<non_emitting_transducer_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<non_emitting_transducer_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если состояние для функции emit для "
    "преобразователя не подходит по сигнатуре", "[emit][transducer]")
{
    CHECK_FALSE(proxima::is_emitting<emitting_transducer_t, double>::value);
    CHECK_FALSE(proxima::is_emitting_v<emitting_transducer_t, double>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если функция emit для преобразователя "
    "имеет неправильную сигнатуру", "[emit][transducer]")
{
    CHECK_FALSE(proxima::is_emitting<wrong_transducer_emit_signature_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<wrong_transducer_emit_signature_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_few_transducer_emit_arguments_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_few_transducer_emit_arguments_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_many_transducer_emit_arguments_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_many_transducer_emit_arguments_t, int>);
}

TEST_CASE("Метафункция \"is_emitting\" возвращает ложь, если преобразователь имеет неправильную"
    "сигнатуру вызова", "[emit][transducer]")
{
    CHECK_FALSE(proxima::is_emitting<too_few_arguments_transducer_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_few_arguments_transducer_t, int>);
    CHECK_FALSE(proxima::is_emitting<too_many_arguments_transducer_t, int>::value);
    CHECK_FALSE(proxima::is_emitting_v<too_many_arguments_transducer_t, int>);
}
