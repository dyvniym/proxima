#include <proxima/state/simple_state.hpp>
#include <proxima/type_traits/is_transducible.hpp>

#include <catch2/catch.hpp>

namespace // anonymous
{
    struct stateless_transducer_t
    {
        template <typename Symbol, typename Tape>
        void operator () (Symbol &&, Tape &&) const
        {
            // Пусто.
        }
    };
}

namespace // anonymous
{
    struct stateful_transducer_t;

    auto initialize (const stateful_transducer_t &, proxima::type_t<long>)
    {
        return proxima::simple_state_t<long>{};
    }

    struct stateful_transducer_t
    {
        template <typename Tape>
        void operator () (long, Tape &&, proxima::simple_state_t<long> &) const
        {
            // Пусто.
        }
    };
}

namespace // anonymous
{
    struct too_few_arguments_stateless_transducer_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }
    };

    struct too_many_arguments_stateless_transducer_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
        }
    };

    struct too_few_arguments_stateful_transducer_t
    {
        template <typename A, typename B>
        void operator () (A &&, B &&) const
        {
        }
    };

    template <typename A>
    auto initialize (const too_few_arguments_stateful_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    struct too_many_arguments_stateful_transducer_t
    {
        template <typename A, typename B, typename C, typename D>
        void operator () (A &&, B &&, C &&, D &&) const
        {
        }
    };

    template <typename A>
    auto initialize (const too_many_arguments_stateful_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
}

TEST_CASE("Метафункция \"is_transducible\" возвращает истину на преобразователе без состояния",
    "[transducible]")
{
    REQUIRE(proxima::is_transducible_v<stateless_transducer_t, int>);
}

TEST_CASE("Метафункция \"is_transducible\" возвращает истину на преобразователе с состоянием",
    "[transducible]")
{
    REQUIRE(proxima::is_transducible_v<stateful_transducer_t, long>);
}

TEST_CASE("Метафункция \"is_transducible\" возвращает ложь на преобразователе с состоянием при "
    "несоответствующем символе", "[transducible]")
{
    REQUIRE_FALSE(proxima::is_transducible_v<stateful_transducer_t, double>);
}

TEST_CASE("Метафункция \"is_transducible\" возвращает ложь на преобразователе с несоответствующей "
    "сигнатурой вызова", "[transducible]")
{
    REQUIRE(not proxima::is_transducible_v<too_few_arguments_stateless_transducer_t, double>);
    REQUIRE(not proxima::is_transducible_v<too_many_arguments_stateless_transducer_t, double>);
    REQUIRE(not proxima::is_transducible_v<too_few_arguments_stateful_transducer_t, double>);
    REQUIRE(not proxima::is_transducible_v<too_many_arguments_stateful_transducer_t, double>);
}

TEST_CASE("Метафункция \"is_transducible\" возвращает ложь на типе, не являющимся "
    "преобразователем", "[transducible]")
{
    REQUIRE_FALSE(proxima::is_transducible_v<double, int>);
}
