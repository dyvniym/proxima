#include <proxima/transducer/window.hpp>
#include <proxima/type.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <cstddef>
#include <deque>
#include <numeric>
#include <type_traits>
#include <utility>

TEST_CASE("Пусть T — тип входного символа оконного преобразователя, тогда тип его выходного "
    "символа — std::deque<T>", "[window][transducer]")
{
    const auto window = proxima::window(10);
    using input_symbol_type = float;
    using output_symbol_type = proxima::output_symbol_t<decltype(window), input_symbol_type>;
    CHECK(std::is_same_v<output_symbol_type, std::deque<input_symbol_type>>);
}

TEST_CASE("Оконный преобразователь ничего не записывает на выходную ленту, пока в общей сумме "
    "с неё прочитано менее N символов, где N — размер окна", "[window][transducer]")
{
    constexpr auto window_size = 5;
    const auto window = proxima::window(window_size);
    auto state = proxima::initialize(window, proxima::type<int>);

    auto tape_call_count = std::size_t{0};
    for (auto symbol = 0; symbol < window_size - 1; ++symbol)
    {
        window(symbol, [& tape_call_count] (auto) {++tape_call_count;}, state);
    }

    CHECK(tape_call_count == 0);
}

TEST_CASE("Каждый символ на входной ленте, начиная с N-го, порождает запись на выходную ленту "
    "контейнера, содержащего последние N из этих символов, где N — размер окна",
    "[window][transducer]")
{
    constexpr auto window_size = 5;
    const auto window = proxima::window(window_size);
    auto state = initialize(window, proxima::type<int>);

    auto symbol = 0;
    for (; symbol < window_size - 1; ++symbol)
    {
        window(symbol, [&] (auto) {}, state);
    }

    for (auto start = 0; symbol < window_size * 2; ++symbol, ++start)
    {
        auto [new_state, output_symbol] = proxima::utility::invoke(window, symbol, state);
        state = std::move(new_state);

        auto expected_result = std::deque<int>(window_size);
        std::iota(expected_result.begin(), expected_result.end(), start);

        CHECK(output_symbol == expected_result);
    }
}
