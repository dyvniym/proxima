#include <proxima/transducer/chunk.hpp>
#include <proxima/type.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/is_emitting.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <cstddef>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

TEST_CASE("Кусочный преобразователь самостоятельно ничего не записывает на выходную ленту, пока в "
    "общей сумме с неё прочитано менее N символов, где N — размер куска", "[chunk][transducer]")
{
    constexpr auto chunk_size = 5;
    const auto chunk = proxima::chunk(chunk_size);
    auto state = proxima::initialize(chunk, proxima::type<int>);

    auto tape_call_count = std::size_t{0};
    for (auto symbol = 0; symbol < chunk_size - 1; ++symbol)
    {
        chunk(symbol, [& tape_call_count] (auto) {++tape_call_count;}, state);
    }

    CHECK(tape_call_count == 0);
}

TEST_CASE("Кусочный преобразователь впервые производит запись на выходную ленту когда со входной "
    "ленты поступает N-й символ", "[chunk][transducer]")
{
    using input_symbol_type = std::string;
    const auto chunk = proxima::chunk(3);
    auto s = proxima::initialize(chunk, proxima::type<input_symbol_type>);

    const auto values = std::vector<std::string>{"1", "2", "3"};
    auto tape_call_count = 0;
    std::for_each(values.begin(), values.end(),
        [& chunk, & s, & tape_call_count] (auto x)
        {
            chunk(x, [& tape_call_count] (auto) {++tape_call_count;}, s);
        });

    CHECK(tape_call_count == 1);
}

TEST_CASE("Тип выходного символа кусочного преобразователя — это \"std::vector\" от типа входного "
    "символа", "[chunk][output_symbol][transducer]")
{
    const auto chunk = proxima::chunk(7);
    using input_symbol_type = float;
    using output_symbol_type = proxima::output_symbol_t<decltype(chunk), input_symbol_type>;
    CHECK(std::is_same_v<output_symbol_type, std::vector<input_symbol_type>>);
}

TEST_CASE("Выходной символ кусочного преобразователя — это контейнер, содержащий N последних "
    "символов со входной ленты", "[chunk][transducer]")
{
    using input_symbol_type = std::string;
    const auto chunk = proxima::chunk(4);
    auto s = proxima::initialize(chunk, proxima::type<input_symbol_type>);

    const auto values = {"1", "2", "3"};
    std::for_each(values.begin(), values.end(),
        [& chunk, & s] (auto input_symbol)
        {
            auto [new_state, output_symbol] = proxima::utility::invoke(chunk, input_symbol, s);
            REQUIRE(output_symbol.empty());
            s = std::move(new_state);
        });

    auto [_, output_symbol] = proxima::utility::invoke(chunk, "4", s);
    static_cast<void>(_);
    CHECK(output_symbol == std::vector<std::string>{"1", "2", "3", "4"});
}

TEST_CASE("Каждый k-й выходной символ — это очередные N символов входной последовательности",
    "[chunk][transducer]")
{
    const auto values = {1, 1, 2, 2, 3, 3, 4, 4};
    const auto chunk = proxima::chunk(2);
    auto state = proxima::initialize(chunk, proxima::type<int>);

    std::vector<std::vector<int>> result;
    std::for_each(values.begin(), values.end(),
        [& chunk, & state, & result] (auto x)
        {
            chunk(x, [& result] (auto y) {result.push_back(std::move(y));}, state);
        });

    CHECK(result == std::vector<std::vector<int>>{{1, 1}, {2, 2}, {3, 3}, {4, 4}});
}

TEST_CASE("Кусочный преобразователь обладает свойством завершения по требованию",
    "[chunk][transducer]")
{
    const auto chunk = proxima::chunk(100500);
    static_assert(proxima::is_emitting_v<decltype(chunk), int>);
    CHECK(proxima::is_emitting_v<decltype(chunk), double>);
}

TEST_CASE("При завершении кусочного преобразователя по требованию из начального состояния на ленту "
    "ничего не записывается", "[chunk][transducer][emit]")
{
    using symbol_type = int;
    auto chunk = proxima::chunk(100500);
    auto initial_state = proxima::initialize(chunk, proxima::type<symbol_type>);

    auto tape_call_count = 0;
    auto tape = [& tape_call_count] (auto &&) {++tape_call_count;};

    proxima::emit(chunk, tape, initial_state);
    CHECK(tape_call_count == 0);
}

TEST_CASE("Если через кусочный преобразователь было пропущено количество элементов, кратное "
    "размеру куска, то при его завершении по требованию на ленту ничего не записывается",
    "[chunk][transducer][emit]")
{
    using symbol_type = long;
    auto chunk = proxima::chunk(5);
    auto state = proxima::initialize(chunk, proxima::type<symbol_type>);

    const auto values = {1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3};
    std::for_each(values.begin(), values.end(),
        [& chunk, & state] (auto x)
        {
            chunk(x, [] (auto) {}, state);
        });

    auto tape_call_count = 0;
    proxima::emit(chunk, [& tape_call_count] (auto) {++tape_call_count;}, state);
    CHECK(tape_call_count == 0);
}

TEST_CASE("При завершении кусочного преобразователя по требованию на выходную ленту записывается "
    "хвост из последних M элементов со входной ленты, если 0 < M < N, N — размер куска",
    "[chunk][transducer][emit]")
{
    using input_symbol_type = char;
    auto chunk = proxima::chunk(3);
    auto s = proxima::initialize(chunk, proxima::type<input_symbol_type>);

    const auto values = {'a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'd', 'd'};
    std::for_each(values.begin(), values.end(),
        [& chunk, & s] (auto x)
        {
            chunk(x, [] (auto) {}, s);
        });

    auto output_symbol = proxima::output_symbol_t<decltype(chunk), input_symbol_type>{};
    proxima::emit(chunk, [& output_symbol] (auto x) {output_symbol = std::move(x);}, s);
    CHECK(output_symbol == std::vector{'d', 'd'});
}
