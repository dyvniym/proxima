#include <proxima/transducer/drop_if.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

TEST_CASE("Преобразователь \"drop_if\" записывает на выходную ленту элементы, не удовлетворяющие "
    "предикату", "[drop_if][transducer]")
{
    const auto t = proxima::drop_if([] (auto x) {return x < 5;});

    const auto x = proxima::utility::invoke(t, 10);
    REQUIRE(x == 10);
}

TEST_CASE("Преобразователь \"drop_if\" не записывает на выходную ленту элементы, удовлетворяющие "
    "предикату", "[drop_if][transducer]")
{
    const auto t = proxima::drop_if([] (auto x) {return x < 5;});

    auto call_count = 0;
    auto tape = [& call_count] (auto) {++call_count;};

    t(3, tape);
    REQUIRE(call_count == 0);
}

TEST_CASE("Преобразователь \"drop_if\" может быть выполнен на этапе компиляции",
    "[drop_if][transducer]")
{
    constexpr auto t = proxima::drop_if([] (auto x) {return x == 'x';});

    constexpr auto x = proxima::utility::invoke(t, 'y');
    static_assert(x == 'y');
    REQUIRE(x == 'y');
}

TEST_CASE("Тип выходного символа преобразователя \"drop_if\" совпадает с типом входного символа",
    "[drop_if][transducer]")
{
    auto t = proxima::drop_if([] (auto x) {return x == 17;});

    using input_symbol_type = short;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}
