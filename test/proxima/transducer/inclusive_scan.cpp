#include <proxima/transducer/inclusive_scan.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <functional>
#include <string>
#include <type_traits>

TEST_CASE("Тип выходного символа преобразователя \"inclusive_scan\" совпадает с типом начального "
    "значения", "[inclusive_scan][transducer]")
{
    using input_symbol_type = int;
    using initial_value_type = double;
    using output_symbol_type =
        proxima::output_symbol_t
        <
            decltype(proxima::inclusive_scan(initial_value_type{0}, std::plus<>{})),
            input_symbol_type
        >;

    CHECK(std::is_same_v<output_symbol_type, initial_value_type>);
}

TEST_CASE("Преобразователь \"inclusive_scan\" на первом шаге возвращает результат применения "
    "функции к начальному значению и первому элементу последовательности",
    "[inclusive_scan][transducer]")
{
    const auto t = proxima::inclusive_scan(3, std::multiplies<>{});
    auto state = proxima::initialize(t, proxima::type<int>);

    auto [_, result] = proxima::utility::invoke(t, 17, state);
    static_cast<void>(_);

    CHECK(result == 51);
}

TEST_CASE("Преобразователь \"inclusive_scan\" возвращает включительную префиксную сумму пройденных "
    "символов относительно начального значения", "[inclusive_scan][transducer]")
{
    const auto t = proxima::inclusive_scan(std::string(u8"ноль"), std::plus<>{});
    auto state0 = proxima::initialize(t, proxima::type<std::string>);

    const auto [state1, symbol1] = proxima::utility::invoke(t, std::string(u8"раз"), state0);
    CHECK(symbol1 == u8"нольраз");
    const auto [state2, symbol2] = proxima::utility::invoke(t, std::string(u8"два"), state1);
    CHECK(symbol2 == u8"нольраздва");
    const auto [state3, symbol3] = proxima::utility::invoke(t, std::string(u8"три"), state2);
    static_cast<void>(state3);
    CHECK(symbol3 == u8"нольраздватри");
}

TEST_CASE("Преобразователь \"inclusive_scan\" умеет работать на этапе компиляции",
    "[inclusive_scan][transducer]")
{
    constexpr auto t = proxima::inclusive_scan(7, [] (auto x, auto y) {return (x + y) % 19;});
    constexpr auto s = proxima::initialize(t, proxima::type<int>);

    constexpr auto r = proxima::utility::invoke(t, 15, s);
    constexpr auto x = r.second;

    static_assert(x == 3);
    CHECK(x == 3);
}
