#include <proxima/transducer/take.hpp>
#include <proxima/type_traits/is_finalizable.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <type_traits>

TEST_CASE("Тип значения состояния \"take\" совпадает с типом \"n\"", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<int>);

    using integral_type = std::decay_t<decltype(n)>;
    REQUIRE(std::is_same_v<integral_type, proxima::state_value_t<decltype(s)>>);
}

TEST_CASE("Тип выходного символа преобразователя \"take\" совпадает с типом входного символа",
    "[take][transducer]")
{
    auto take = proxima::take(10);

    using input_symbol_type = long;
    using output_symbol_type = proxima::output_symbol_t<decltype(take), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Преобразователь \"take\" финализируем", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<int>);
    REQUIRE(proxima::is_finalizable_v<decltype(take), decltype(s)>);
}

TEST_CASE("Начальное состояние \"take\" равно количеству символов, которые надо взять", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<int>);
    REQUIRE(proxima::value(s) == n);
}

TEST_CASE("Взятие символа уменьшает значение состояния ровно на единицу", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<char>);

    take('x', [] (auto) {}, s);
    REQUIRE(proxima::value(s) == n - 1);
}

TEST_CASE("Взятие максимально возможного числа символов приводит к тому, что, "
    "значение состояния равно нулю", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        take('x', [] (auto) {}, s);
    }

    REQUIRE(proxima::value(s) == 0);
}

TEST_CASE("Если не все символы были взяты, то преобразователь \"take\""
    " не в финальном состоянии", "[take]")
{
    const auto n = 2;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<char>);

    take('x', [] (auto) {}, s);
    REQUIRE(not proxima::is_final(take, s));
}

TEST_CASE("Если максимально возможное число символов было взято, то \"take\" находится в финальном"
    " состоянии", "[take]")
{
    const auto n = 10;
    auto take = proxima::take(n);
    auto s = proxima::initialize(take, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        take('x', [] (auto) {}, s);
    }

    REQUIRE(proxima::is_final(take, s));
}

TEST_CASE("Взятие символа можно выполнить на этапе компиляции", "[take]")
{
    constexpr auto n = 1;
    constexpr auto take = proxima::take(n);
    constexpr auto s = proxima::initialize(take, proxima::type<char>);

    constexpr auto r = proxima::utility::invoke(take, 'x', s);
    constexpr auto x = r.second;
    static_assert(x == 'x');
    REQUIRE(x == 'x');
}
