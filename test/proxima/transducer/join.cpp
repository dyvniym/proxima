#include <proxima/compose.hpp>
#include <proxima/kernel/to_vector.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/join.hpp>
#include <proxima/transducer/take.hpp>
#include <proxima/type_traits/output_symbol.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <iterator>
#include <list>
#include <type_traits>
#include <vector>

TEST_CASE("Тип значения выходного символа \"join\" совпадает"
    " с \"value_type\" входного диапазона", "[join]")
{
    auto symbol = std::vector<int>{1, 2, 3};
    auto join = proxima::join;

    using input_symbol = std::iterator_traits<decltype(symbol)::iterator>::value_type;
    using output_symbol = proxima::output_symbol_t<decltype(join), decltype(symbol)>;

    REQUIRE(std::is_same_v<input_symbol, output_symbol>);
}

TEST_CASE("Символа-диапазон будет расплющен джойном", "[join]")
{
    auto ranges = std::vector<std::list<int>>{{1, 2, 3}};
    auto join = proxima::join;
    auto to_vector = proxima::to_vector;

    auto composite = proxima::compose(join, to_vector);
    auto result = proxima::reduce(ranges, composite);

    auto expected = {1, 2, 3};
    REQUIRE(std::equal(expected.begin(), expected.end(), std::begin(result)));
}

TEST_CASE("При размотке лишние символы не будут взяты", "[join]")
{
    auto ranges = std::vector<std::list<int>>{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    auto join = proxima::join;
    auto take = proxima::take(5);
    auto to_vector = proxima::to_vector;

    auto composite = proxima::compose(join, take, to_vector);
    auto result = proxima::reduce(ranges, composite);

    auto expected = {1, 2, 3, 4, 5};
    REQUIRE(std::equal(expected.begin(), expected.end(), std::begin(result)));
}
