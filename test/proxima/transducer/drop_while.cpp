#include <proxima/transducer/drop_while.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <string>

TEST_CASE("Преобразователь \"drop_while\" игнорирует символы входной ленты, удовлетворяющие "
    "предикату", "[drop_while][transducer]")
{
    const auto t = proxima::drop_while([] (auto x) {return x.size() == 0;});
    auto s = proxima::initialize(t, proxima::type<std::string>);

    auto tape_call_count = 0;
    auto tape = [& tape_call_count] (auto) {++tape_call_count;};

    t(std::string{}, tape, s);
    t(std::string{}, tape, s);
    t(std::string{}, tape, s);

    REQUIRE(tape_call_count == 0);
}

TEST_CASE("Преобразователь \"drop_while\" записывает на выходную ленту символы, не удовлетворяющие "
    "предикату", "[drop_while][transducer]")
{
    const auto t = proxima::drop_while([] (auto x) {return x.size() == 0;});
    const auto s = proxima::initialize(t, proxima::type<std::string>);

    const auto [_, x] = proxima::utility::invoke(t, std::string("qwe"), s);
    static_cast<void>(_);

    REQUIRE(x == "qwe");
}

TEST_CASE("Начиная с первого символа, не удовлетворяющего предикату, преобразователь \"drop_while\" "
    "записывает на выходную ленту все символы без разбора", "[drop_while][transducer]")
{
    const auto t = proxima::drop_while([] (auto x) {return x < 3;});
    auto s = proxima::initialize(t, proxima::type<int>);

    auto tape_call_count = 0;
    auto tape = [& tape_call_count] (auto) {++tape_call_count;};

    t(0, tape, s); // -
    t(1, tape, s); // -
    t(2, tape, s); // -
    t(3, tape, s); // +
    t(0, tape, s); // +
    t(1, tape, s); // +
    t(2, tape, s); // +

    REQUIRE(tape_call_count == 4);
}

TEST_CASE("Тип выходного символа преобразователя \"drop_while\" совпадает с типом входного символа",
    "[drop_while][transducer]")
{
    auto t = proxima::drop_while([] (auto x) {return x == 17;});

    using input_symbol_type = short;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Преобразователь \"drop_while\" может быть выполнен на этапе компиляции",
    "[drop_while][transducer]")
{
    constexpr auto t = proxima::drop_while([] (auto x) {return x == 'x';});
    constexpr auto s = proxima::initialize(t, proxima::type<char>);

    constexpr auto r = proxima::utility::invoke(t, 'y', s);
    constexpr auto x = r.second;
    static_assert(x == 'y');
    REQUIRE(x == 'y');
}
