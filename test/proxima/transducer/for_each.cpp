#include <proxima/transducer/for_each.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <functional>
#include <type_traits>

TEST_CASE("Преобразователь \"for_each\" записывает на выходную ленту тот же символ, который "
    "считывает со входной, в случае, если заданная функция не изменяет входной символ",
    "[for_each][transducer]")
{
    const auto value = 2;
    const auto t = proxima::for_each([] (auto) {});
    const auto result = proxima::utility::invoke(t, value);

    REQUIRE(result == value);
}

TEST_CASE("Преобразователь \"for_each\" записывает на выходную ленту модифицированный входной "
    "символ, в случае, если заданная функция изменяет входной символ", "[for_each][transducer]")
{
    const auto value = 2;
    const auto t = proxima::for_each([] (auto & x) {++x;});
    const auto result = proxima::utility::invoke(t, value);

    REQUIRE(result == value + 1);
}

TEST_CASE("Преобразователь \"for_each\" может быть выполнен на этапе компиляции ",
    "[for_each][transducer]")
{
    constexpr auto value = 2;
    constexpr auto t = proxima::for_each([] (auto & x) {++x;});
    constexpr auto result = proxima::utility::invoke(t, value);

    static_assert(result == value + 1);
    REQUIRE(result == value + 1);
}

TEST_CASE("Тип выходного символа преобразователя \"for_each\" совпадает с типом входного символа",
    "[for_each][transducer]")
{
    auto t = proxima::for_each([] (auto & x) {++x;});

    using input_symbol_type = long;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}
