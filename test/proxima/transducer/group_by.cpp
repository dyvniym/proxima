#include <proxima/transducer/group_by.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <functional>
#include <iterator>
#include <type_traits>

namespace // anonymous
{
    template <typename>
    struct is_std_vector: std::false_type {};

    template <typename Value>
    struct is_std_vector<std::vector<Value>>: std::true_type {};
} // namespace anonymous

TEST_CASE("Перегрузка \"output_symbol\" у \"group_by\"", "[group_by] [output_symbol]")
{
    // не const — см. output_symbol перегрузки
    auto group_by = proxima::group_by(std::equal_to<>{});
    SECTION("определена и возвращает \"std::vector\" от указанного типа символа")
    {
        using symbol_type = float;
        using output_symbol = proxima::output_symbol_t<decltype(group_by), symbol_type>;
        REQUIRE(std::is_same_v<output_symbol, std::vector<symbol_type>>);
    }
}

TEST_CASE("Тип значения сосотояния \"group_by\" — \"std::vector\"", "[group_by]")
{
    using symbol_type = int;
    const auto s =
        proxima::initialize(proxima::group_by(std::equal_to<>{}), proxima::type<symbol_type>);
    using state_value_type = proxima::state_value_t<decltype(s)>;
    REQUIRE(is_std_vector<state_value_type>::value);

    SECTION("причем тип хранимых внутри элементов устанавливается вызовом \"initialize\"")
    {
        REQUIRE(std::is_same_v<symbol_type, proxima::state_symbol_t<decltype(s)>>);
    }
}

TEST_CASE("Значение начального сосотояния \"group_by\" — пустой контейнер", "[group_by]")
{
    using symbol_type = int;
    auto s = proxima::initialize(proxima::group_by(std::equal_to<>{}), proxima::type<symbol_type>);
    REQUIRE(std::empty(proxima::value(s)));
}

TEST_CASE("Значение промежуточного состояния \"group_by\" — непустой контейнер", "[group_by]")
{
    using symbol_type = int;
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto s = proxima::initialize(group_by, proxima::type<symbol_type>);

    const auto values = {1, 1, 1};
    std::for_each(values.begin(), values.end(),
        [& group_by, & s] (auto x)
        {
            group_by(x, [] (auto) {}, s);
        });

    REQUIRE(not std::empty(proxima::value(s)));
    REQUIRE(std::size(proxima::value(s)) == std::size(values));
}

TEST_CASE("Значение состояния \"group_by\" после вызова \"emit\" — пустой контейнер", "[group_by]")
{
    using symbol_type = int;
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto s = proxima::initialize(group_by, proxima::type<symbol_type>);

    const auto values = {1, 1, 1};
    std::for_each(values.begin(), values.end(),
        [& group_by, & s] (auto x)
        {
            group_by(x, [] (auto) {}, s);
        });

    proxima::emit(group_by, [] (auto) {}, s);
    REQUIRE(std::empty(value(s)));
}

TEST_CASE("Эквивалентные (в смысле предиката) символы скапливаются в сосотоянии", "[group_by]")
{
    using symbol_type = int;
    constexpr auto p = std::equal_to<>{};
    auto group_by = proxima::group_by(p);
    auto s = proxima::initialize(group_by, proxima::type<symbol_type>);

    const auto values = {1, 1, 1, 2, 2, 2, 2, -1, -1, -1, 0};
    std::for_each(values.begin(), values.end(),
        [& group_by, & s] (auto x)
        {
            group_by
            (
                x,
                [] (auto v)
                {
                    auto x = v.front();
                    REQUIRE(std::all_of(std::begin(v), std::end(v), [x] (auto s) {return s == x;}));
                },
                s
            );
        });
}

TEST_CASE("При поступлении несовпадающего (в смысле предиката) символа, "
    "накопленные будут выброшены из состояния", "[group_by]")
{
    using symbol_type = int;
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto s = proxima::initialize(group_by, proxima::type<symbol_type>);

    const auto values = {1, 1, 1, 2};
    std::for_each(values.begin(), values.end(),
        [& group_by, & s] (auto x)
        {
            group_by(x, [] (auto) {}, s);
        });

    const auto & result = proxima::value(s);
    REQUIRE(std::find(std::begin(result), std::end(result), 1) == std::end(result));
    SECTION("а сам этот символ сохраняется внутри состояния")
    {
        REQUIRE(std::size(result) == 1ul);
        REQUIRE(result.front() == 2);
    }
}

TEST_CASE("Вызов \"emit\" из начального сосотояния не записывает на ленту", "[group_by]")
{
    using symbol_type = int;
    auto group_by = proxima::group_by(std::equal_to<>{});
    auto initial_state = proxima::initialize(group_by, proxima::type<symbol_type>);

    auto ncalls = 0;
    auto tape = [& ncalls] (auto &&) {++ncalls;};

    proxima::emit(group_by, tape, initial_state);
    REQUIRE(ncalls == 0);
}
