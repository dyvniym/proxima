#include <proxima/thread/thread_pool.hpp>

#include <catch2/catch.hpp>

#include <algorithm>
#include <atomic>
#include <cstddef>
#include <future>
#include <iterator>
#include <random>
#include <stdexcept>
#include <thread>
#include <vector>

TEST_CASE("Метод try_post возвращает пару из std::future, содержащего отложенный результат "
    "отправленной задачи, и булевого значения, содержащего индикатор успеха отправки задачи в "
    "очередь", "[thread_pool][thread]")
{
    proxima::thread_pool p(10);

    auto maybe_future = p.try_post([] {return 7;});

    CHECK(maybe_future.has_value());
    CHECK(maybe_future->get() == 7);
}

TEST_CASE("Остановка пула потоков приводит к невозможности поставить в очередь новую задачу, "
    "и метод try_post станет возвращать пару из пустой std::future и булевой лжи",
    "[thread_pool][thread]")
{
    proxima::thread_pool p(4);
    p.stop();

    auto maybe_future = p.try_post([] {return 7;});

    CHECK_FALSE(maybe_future.has_value());
}

TEST_CASE("Вызов wait() приводит к невозможности поставить в очередь новую задачу, "
    "а метод try_post станет возвращать пару из пустой std::future и булевой лжи",
    "[thread_pool][thread]")
{
    proxima::thread_pool p(4);
    p.wait();

    auto maybe_future = p.try_post([] {return 7;});

    CHECK_FALSE(maybe_future.has_value());
}

TEST_CASE("Пул потоков обладает возможностью моментальной остановки. При этом задачи, сидящие в "
    "очереди, снимаются, а соответствующие им std::future протухают", "[thread_pool][thread]")
{
    constexpr auto task_count = 10u;
    constexpr auto thread_count = 4u;
    static_assert(thread_count < task_count);

    auto p = proxima::thread_pool(thread_count);

    auto stopped = std::atomic<bool>{false};
    auto call_count = std::atomic<std::size_t>{0};
    auto pending_task =
        [& stopped, & call_count]
        {
            while (not stopped) {};
            ++call_count;
        };

    std::vector<std::future<void>> futures;
    std::generate_n(std::back_inserter(futures), task_count,
        [& p, & pending_task] {return p.post(pending_task);});

    p.stop();
    stopped = true;

    auto exception_count = std::size_t{0};
    for (auto & future: futures)
    {
        try
        {
            future.get();
        }
        catch (const std::future_error &)
        {
            ++exception_count;
        }
    }

    CHECK(call_count <= thread_count);
    CHECK(exception_count >= (task_count - thread_count));
    CHECK((call_count + exception_count) == task_count);
}

TEST_CASE("Вызов stop() не блокирует вызывающий поток", "[thread_pool][thread]")
{
    auto p = proxima::thread_pool(4);

    auto stopped = std::atomic<bool>{false};
    auto launched = std::atomic<bool>{false};
    p.post([& launched, & stopped]
        {
            launched = true;
            while (not stopped);
        });

    while (not launched);
    p.stop();
    stopped = true;

    CHECK(stopped);
}

TEST_CASE("Вызов wait() блокирует вызывающий поток до тех пор, пока не завершатся все задачи, "
    "которые на момент вызова уже были в очереди", "[thread_pool][thread]")
{
    constexpr auto task_count = 10u;
    constexpr auto thread_count = 4u;
    static_assert(thread_count < task_count);

    auto p = proxima::thread_pool(thread_count);

    auto queue_is_closed = std::atomic<bool>{false};
    auto call_count = std::atomic<std::size_t>{0};
    auto pending_task =
        [& queue_is_closed, & call_count]
        {
            while (not queue_is_closed) {};
            ++call_count;
        };

    std::vector<std::future<void>> futures;
    std::generate_n(std::back_inserter(futures), task_count,
        [& p, & pending_task] {return p.post(pending_task);});

    REQUIRE(call_count == 0);

    // Нужно гарантировать, чтобы часть задач в момент вызова wait() была всё ещё в очереди.
    std::thread([& queue_is_closed, & p]
        {
            // Когда в пул нельзя ничего положить, это значит, что либо он полностью остановлен,
            // либо закрыта очередь. В нашем случае — второй вариант, потому что метод stop()
            // здесь нигде не зовётся.
            // Согласен, способ дураковатый, но другого я не вижу ¯\_(ツ)_/¯ .
            while (p.try_post([]{}).has_value()) {}
            queue_is_closed = true;
        }).detach();

    p.wait();

    CHECK(call_count == task_count);
}

TEST_CASE("Допустимы несколько последовательных вызовов wait()", "[thread_pool][thread]")
{
    auto p = proxima::thread_pool(4);

    auto has_finished = std::atomic<bool>{false};
    p.post([& has_finished] {has_finished = true;});

    p.wait();
    p.wait();
    p.wait();

    CHECK(has_finished);
}

TEST_CASE("Результат вызова wait() после stop() логически эквивалентен результату одного только "
    "вызова stop()", "[thread_pool][thread]")
{
    constexpr auto task_count = 10u;
    constexpr auto thread_count = 4u;
    static_assert(thread_count < task_count);

    auto p = proxima::thread_pool(thread_count);

    auto stopped = std::atomic<bool>{false};
    auto call_count = std::atomic<std::size_t>{0};
    auto pending_task =
        [& stopped, & call_count]
        {
            while (not stopped) {};
            ++call_count;
        };

    std::vector<std::future<void>> futures;
    std::generate_n(std::back_inserter(futures), task_count,
        [& p, & pending_task] {return p.post(pending_task);});

    p.stop();
    stopped = true;
    p.wait();

    auto exception_count = std::size_t{0};
    for (auto & future: futures)
    {
        try
        {
            future.get();
        }
        catch (const std::future_error &)
        {
            ++exception_count;
        }
    }

    CHECK(call_count <= thread_count);
    CHECK(exception_count >= (task_count - thread_count));
    CHECK((call_count + exception_count) == task_count);
}

TEST_CASE("Каждая задача выполняется в потоке, отличном от основного", "[thread_pool][thread]")
{
    const auto no_thread_id = std::thread::id{};
    const auto this_thread_id = std::this_thread::get_id();

    std::vector<std::future<std::thread::id>> ids;
    proxima::thread_pool pool(std::max(std::thread::hardware_concurrency(), 1u));

    for (auto i = 0; i < 100; ++i)
    {
        ids.push_back(pool.post([]
        {
            return std::this_thread::get_id();
        }));
    }

    for (auto & id: ids)
    {
        const auto thread_id = id.get();
        CHECK(thread_id != no_thread_id);
        CHECK(thread_id != this_thread_id);
    }
}

TEST_CASE("Пул потоков может быть остановлен из задачи, выполняющейся в этом же пуле потоков",
    "[thread_pool][thread]")
{
    proxima::thread_pool pool(4);

    auto f =
        pool.post([& pool]
        {
            pool.stop();
            return 17;
        });

    CHECK(f.get() == 17);
}
