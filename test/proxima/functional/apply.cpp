#include <proxima/functional/apply.hpp>

#include <catch2/catch.hpp>

#include <cstddef>
#include <functional>
#include <tuple>

namespace // anonymous
{
    struct dummy
    {
        dummy ()
        {
            ++instances_count;
        }

        dummy (const dummy &)
        {
            ++instances_count;
        }

        dummy (dummy &&)
        {
            ++instances_count;
        }

        ~dummy ()
        {
            --instances_count;
        }

        static int instances_count;
    };
    int dummy::instances_count = 0;

    struct const_lvalue_caller
    {
        const_lvalue_caller (std::size_t & calls):
            calls(calls)
        {
        }

        template <typename ... T>
        auto operator () (T...) const &
        {
            ++calls;
            return true;
        }

        template <typename ... T>
        auto operator () (T...) & = delete;

        template <typename ... T>
        auto operator () (T...) && = delete;

        std::size_t & calls;
    };

    struct lvalue_caller
    {
        lvalue_caller (std::size_t & calls):
            calls(calls)
        {
        }

        template <typename ... T>
        auto operator () (T...) const & = delete;

        template <typename ... T>
        auto operator () (T...) &
        {
            ++calls;
            return true;
        }

        template <typename ... T>
        auto operator () (T...) && = delete;

        std::size_t & calls;
    };

    struct rvalue_caller
    {
        rvalue_caller (std::size_t & calls):
            calls(calls)
        {
        }

        template <typename ... T>
        auto operator () (T...) const & = delete;

        template <typename ... T>
        auto operator () (T...) & = delete;

        template <typename ... T>
        auto operator () (T...) &&
        {
            ++calls;
            return true;
        }

        std::size_t & calls;
    };
} // namespace anonymous

TEST_CASE("Применяет n-местную функцию к поэлементно развёрнутому кортежу размера n",
    "[functional][apply]")
{
    auto a = proxima::apply(std::plus<>{});
    auto r = a(std::make_tuple(1, 2));
    CHECK(r == 3);
}

TEST_CASE("Хранит копию переданного функционального объекта", "[functional][apply]")
{
    const auto old_instances_count = dummy::instances_count;

    auto a = proxima::apply(dummy{});
    static_cast<void>(a);

    CHECK(dummy::instances_count == old_instances_count + 1);
}

TEST_CASE("Не хранит экземпляр функционального объекта, переданного через std::ref",
    "[functional][apply]")
{
    auto d = dummy{};
    const auto old_instances_count = dummy::instances_count;

    auto a = proxima::apply(std::ref(d));
    static_cast<void>(a);

    CHECK(dummy::instances_count == old_instances_count);
}

TEST_CASE("Хранимый функциональный объект вызывается как константный lvalue, когда сам apply "
    "константный lvalue", "[functional][apply]")
{
    auto calls = std::size_t{0};
    const auto a = proxima::apply(const_lvalue_caller{calls});

    a(std::make_tuple(1, "qwe"));

    CHECK(calls == 1);
}

TEST_CASE("Хранимый функциональный объект вызывается как lvalue, когда сам apply — lvalue",
    "[functional][apply]")
{
    auto calls = std::size_t{0};
    auto a = proxima::apply(lvalue_caller{calls});

    a(std::make_tuple(1, "qwe"));

    CHECK(calls == 1);
}

TEST_CASE("Хранимый функциональный объект вызывается как rvalue, когда сам apply — rvalue",
    "[functional][apply]")
{
    auto calls = std::size_t{0};
    auto c = rvalue_caller{calls};

    proxima::apply(c)(std::make_tuple(1, "qwe"));

    CHECK(calls == 1);
}

TEST_CASE("Функциональный объект, переданный через std::ref, вызывается как lvalue",
    "[functional][apply]")
{
    auto calls = std::size_t{0};
    auto f = lvalue_caller{calls};

    proxima::apply(std::ref(f))(std::make_tuple(1, "qwe"));

    CHECK(calls == 1);
}

TEST_CASE("Константный функциональный объект, переданный через std::ref, вызывается как "
    "константный lvalue", "[functional][apply]")
{
    auto calls = std::size_t{0};
    const auto f = const_lvalue_caller{calls};

    proxima::apply(std::ref(f))(std::make_tuple(1, "qwe"));

    CHECK(calls == 1);
}

TEST_CASE("apply может быть вычислен на этапе компиляции", "[functional][apply]")
{
    constexpr auto a = proxima::apply(std::multiplies<>{});
    constexpr auto r = a(std::make_tuple(4, 5));
    static_assert(r == 20);
    CHECK(r == 20);
}
