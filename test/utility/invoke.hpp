#pragma once

#include <proxima/type_traits/is_reducible.hpp>
#include <proxima/type_traits/is_transducible.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_symbol.hpp>

#include <type_traits>
#include <utility>

namespace proxima::utility
{
    template <typename Transducer, typename Symbol, typename =
        std::enable_if_t<is_transducible_v<Transducer, Symbol>>>
    constexpr auto invoke (Transducer tr, Symbol x)
        -> output_symbol_t<Transducer, Symbol>
    {
        using output_symbol_type = output_symbol_t<Transducer, Symbol>;
        auto s = output_symbol_type{};
        tr(x, [& s] (auto x) {s = x;});
        return s;
    }

    template <typename Transducer, typename Symbol, typename State, typename =
        std::enable_if_t
        <
            is_transducible_v<Transducer, state_symbol_t<State>> &&
            std::is_convertible_v<Symbol, state_symbol_t<State>>
        >>
    constexpr auto invoke (Transducer tr, Symbol x, State st)
        -> std::pair<State, output_symbol_t<Transducer, state_symbol_t<State>>>
    {
        using output_symbol_type = output_symbol_t<Transducer, state_symbol_t<State>>;
        auto s = output_symbol_type{};
        tr(x, [& s] (auto x) {s = x;}, st);
        return std::pair(st, s);
    }

    template <typename Kernel, typename Symbol, typename State, typename =
        std::enable_if_t
        <
            is_reducible_v<Kernel, state_symbol_t<State>> &&
            std::is_convertible_v<Symbol, state_symbol_t<State>>
        >>
    constexpr auto invoke (Kernel k, Symbol x, State st)
    {
        k(x, st);
        return st;
    }
} // namespace proxima::utility
