#pragma once

#include <proxima/functional/not_fn.hpp>
#include <proxima/transducer/take_if.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Creates the negative filtering transducer

            \param p
                A predicate that will indicate if the symbol from the input tape should be dropped
                or not.

            \returns
                An instance of `take_if_t` with inverted predicate `p`.

        \~russian
            \brief
                Создание отрицательно фильтрующего преобразователя

            \param p
                Символы со входной ленты, удовлетворяющие предикату, будут проигнорированы, а
                символы, не удовлетворяющие предикату, будут записаны на выходную ленту
                преобразователя.

            \returns
                Экземпляр `take_if_t` с инвертированным предикатом `p`.

        \~  \see take_if_t
            \see take_if
     */
    template <typename UnaryPredicate>
    constexpr auto drop_if (UnaryPredicate && p)
    {
        return take_if(not_fn(std::forward<UnaryPredicate>(p)));
    }
}
