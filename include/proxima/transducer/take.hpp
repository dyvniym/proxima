#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <utility>
#include <cassert>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that takes the prefix of the input sequence of the given length

            \tparam Integral
                The type of the counter.

        \~russian
            \brief
                Преобразователь, отбирающий из входной последовательности префикс заданной длины

            \tparam Integral
                Целочисленный тип. В этих единицах ведётся подсчёт символов.

        \~  \see take
            \see take_if
            \see take_while_t
            \see drop_t
            \see drop_if_t
            \see drop_while_t
     */
    template <typename Integral>
    struct take_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape, writes it to the output tape and decreases
                    the counter of the remaining symbols.

                \pre
                    `value(state) > 0`

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Переписывает символ со входой ленты на выходную, а также уменьшает счётчик
                    оставшихся элементов.

                \pre
                    `value(state) > 0`

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename Symbol, typename UnaryFunction, typename State>
        constexpr void operator () (Symbol && symbol, UnaryFunction && tape, State & state) const
        {
            assert(value(state) > Integral{0});
            std::forward<UnaryFunction>(tape)(std::forward<Symbol>(symbol));
            --value(state);
        }

        Integral n;
    };

    template <typename Integral, typename T>
    constexpr auto initialize (const take_t<Integral> & take, type_t<T>)
    {
        return simple_state_t<Integral, T>{take.n};
    }

    /*!
        \~english
            \brief
                Creates a prefix-taking transducer

            \param n
                The amount of symbols to take.

            \pre
                `n >= 0`

            \returns
                An instance of `take_t`.

        \~russian
            \brief
                Создать преобразователь, отбирающий префикс заданной длины

            \param n
                Количество символов, которое требуется отобрать.

            \pre
                `n >= 0`

            \returns
                Экземпляр `take_t`.

        \~  \see take_t
            \see drop
            \see take_if
            \see drop_if
            \see take_while
            \see drop_while
     */
    template <typename Integral>
    constexpr auto take (Integral n) -> take_t<Integral>
    {
        static_assert(std::is_integral_v<Integral>);
        assert(n >= Integral{0});
        return {n};
    }

    template <typename Integral, typename State>
    constexpr bool is_final (const take_t<Integral> &, const State & st)
    {
        return value(st) == Integral{0};
    }
}
