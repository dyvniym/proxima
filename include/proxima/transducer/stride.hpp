#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cassert>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that takes each `n`th symbol of the sequence

            \tparam Integer
                A counter type.

        \~russian
            \brief
                Преобразователь, отбирающий каждый `n`-й символ последовательности

            \tparam Integer
                Тип счётчика.

        \~  \see stride
            \see take_t
            \see drop_t
     */
    template <typename Integer>
    struct stride_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and if its index is `nk`th (indexing is
                    null-based, `n > 0`, `k = 0, 1, 2, ...`) then writes it to the output tape.
                    Otherwise the symbol is ignored, i.e. nothing is written to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и, если этот символ является `nk`-м по порядку
                    символом, поступившим со входной ленты (отсчёт начинается с нуля, `n > 0`,
                    `k = 0, 1, ...`), то записывает его на выходную ленту. В противном случае символ
                    игнорируется, то есть на выходную ленту ничего не записывается.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (value(state) == Integer{0})
            {
                value(state) = n;
                std::forward<T>(tape)(std::forward<A>(symbol));
            }

            --value(state);
        }

        Integer n;
    };

    /*!
        \~english
            \brief
                Creates the transducer that takes each `n`th symbol of the sequence

            \param n
                The step of the selection.

            \pre
                `n > 0`

            \returns
                An instance of `stride_t`.

        \~russian
            \brief
                Создать преобразователь, отбирающий каждый `n`-й символ последовательности

            \param n
                Шаг, с которым будут браться элементы последовательности.

            \pre
                `n > 0`

            \returns
                Экземпляр `stride_t`.

        \~  \see stride_t
            \see take
            \see drop
     */
    template <typename Integer>
    constexpr auto stride (Integer n)
    {
        static_assert(std::is_integral_v<Integer>);
        assert(n > 0);
        return stride_t<Integer>{n};
    }

    template <typename Integer, typename Symbol>
    constexpr auto initialize (const stride_t<Integer> &, type_t<Symbol>)
    {
        return simple_state_t<Integer, Symbol>{0};
    }
} // namespace proxima
