#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cstddef>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The enumerating transducer

            \details
                Writes to the output tape a symbol read from the input tape with its number
                attached to it:

                \f[
                s_i^{output} = (s_i^{input}, c_0 + i),
                \f]

                where \f$c_0\f$ is the initial value of the counter, \f$s_i^{input}\f$ is \f$i\f$th
                symbol read from the input tape, \f$s_i^{output}\f$ is \f$i\f$th symbol written to
                the output tape.

            \tparam Integral
                The type of the counter.

        \~russian
            \brief
                Нумерующий преобразователь

            \details
                Записывает на выходную ленту символ, прочитанный со входной ленты с приклеенным к
                нему номером:

                \f[
                s_i^{output} = (s_i^{input}, c_0 + i)
                \f]

                где \f$c_0\f$ — начальное значение счётчика, \f$s_i^{input}\f$ — \f$i\f$-й символ со
                входной ленты, \f$s_i^{output}\f$ — \f$i\f$-й символ на выходной ленте.

            \tparam Integral
                Тип счётчика.

        \~  \see enumerate
     */
    template <typename Integral>
    struct enumerate_t
    {
        template <typename A>
        using output_symbol_t = std::pair<A, Integral>;

        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape, attaches the current value of the counter
                    to it, writes the resulting pair to the output tape and increments the counter.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты, приклеивает к нему текущее значение счётчика,
                    записывает полученную пару на выходную ленту и увеличивает счётчик.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            std::forward<T>(tape)(std::pair(std::forward<A>(symbol), value(state)));
            ++value(state);
        }

        /*!
            \~english
                \brief
                    Creates an enumerating transducer with custom counter

                \param n
                    The initial value of the custom counter.

                \tparam Integral1
                    The type of the custom counter.

                \returns
                    An instance of `enumerate_t` with the custom counter.

            \~russian
                \brief
                    Создать нумерующий преобразователь с пользовательским счётчиком

                \param n
                    Начальное значение счётчика.

                \tparam Integral1
                    Тип пользовательского счётчика.

                \returns
                    Экземпляр преобразователя `enumerate_t` с пользовательским счётчиком.
         */
        template <typename Integral1>
        constexpr auto operator () (Integral1 n) const
        {
            return enumerate_t<Integral1>{n};
        }

        Integral initial;
    };

    /*!
        \~english
            \brief
                A convenience instrument to use in `compose` function

            \details
                Usage examples:

                \code{.cpp}
                auto k = proxima::compose(proxima::enumerate, proxima::to_vector);
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::enumerate(10),
                        protima::transform(proxima::apply(std::multiplies<>{})),
                        proxima::sum
                    );
                \endcode

            \returns
                An instance of `enumerate_t` transducer.

        \~russian
            \brief
                Инструмент для использования в функции `compose`

            \details
                Примеры использования:

                \code{.cpp}
                auto k = proxima::compose(proxima::enumerate, proxima::to_vector);
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::enumerate(10),
                        protima::transform(proxima::apply(std::multiplies<>{})),
                        proxima::sum
                    );
                \endcode

            \returns
                Экземпляр преобразователя `enumerate_t`.

        \~  \see enumerate_t
            \see compose
     */
    constexpr auto enumerate = enumerate_t<std::size_t>{0};

    template <typename Integral, typename A>
    constexpr auto initialize (const enumerate_t<Integral> & t, type_t<A>)
    {
        return simple_state_t<Integral, A>{t.initial};
    }
}
