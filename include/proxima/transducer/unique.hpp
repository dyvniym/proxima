#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <functional>
#include <optional>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that leaves only unique symbols

            \tparam BinaryPredicate
                The type of the predicate that will check for uniqueness of symbols.

        \~russian
            \brief
                Преобразователь, оставляющий только неповторяющиеся символы

            \tparam BinaryPredicate
                Тип предиката, который будет проверять уникальность символов.

        \~  \see unique
     */
    template <typename BinaryPredicate>
    struct unique_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if it is identical to the previous
                    one or not. If it is, then the symbol is ignored, i.e. transducer does not write
                    it to the output tape. If the symbol is equal to previous or it is the first
                    symbol that has been read from the input tape, then the transducer saves it to
                    the state and then writes it to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и с помощью заданного предиката проверяет,
                    является ли этот символ идентичным предыдущему, или нет. Если является, то
                    игнорирует его, то есть не пишет на выходную ленту ничего. Если же символ не
                    равен предыдущему или является первым в последовательности (то есть до него не
                    было считано ни одного символа со входной ленты), то преобразователь сохраняет
                    этот символ в своём состоянии, а затем записывает его на выходную ленту.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (not value(state).has_value() ||
                not equal(std::as_const(*value(state)), std::as_const(symbol)))
            {
                value(state).emplace(std::forward<A>(symbol));
                std::forward<T>(tape)(*value(state));
            }
        }

        /*!
            \~english
                \brief
                    Creates a transducer with custom predicate

                \param p
                    The predicate that will be used to check for equality of symbols.

                \returns
                    An instance of `unique_t` with custom predicate.

            \~russian
                \brief
                    Создать преобразователь с пользовательским предикатом

                \param p
                    Предикат, с помощью которого будет производиться проверка символов на равенство.

                \returns
                    Экземпляр преобразователя `unique_t` с заданным предикатом.
         */
        template <typename BinaryPredicate1>
        constexpr auto operator () (BinaryPredicate1 && p) const
            -> unique_t<std::decay_t<BinaryPredicate1>>
        {
            return {std::forward<BinaryPredicate1>(p)};
        }

        BinaryPredicate equal;
    };

    /*!
        \~english
            \brief
                A convenience instrument to use in `compose` function

            \details
                Usage examples:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique([] (auto x, auto y) {return x.size() == y.size();}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                An instance of `unique_t` transducer.

        \~russian
            \brief
                Инструмент для использования в функции `compose`

            \details
                Примеры использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique([] (auto x, auto y) {return x.size() == y.size();}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                Экземпляр преобразователя `unique_t`.

        \~  \see unique_t
            \see compose
     */
    constexpr auto unique = unique_t<std::equal_to<>>{};

    template <typename BinaryPredicate, typename Symbol>
    constexpr auto initialize (const unique_t<BinaryPredicate> &, type_t<Symbol>)
    {
        return simple_state_t<std::optional<Symbol>, Symbol>{};
    }
}
