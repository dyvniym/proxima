#pragma once

#include <proxima/type_traits/is_finalizable.hpp>
#include <proxima/type_traits/state_symbol.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Generalized finality check

            \tparam K
                The type of the reduce kernel which is required to be checked for finality.
            \tparam S
                The type of the state of `kernel`.

            \returns
                `true` if and only if `K` is finalizable over the state `S` and `state` is final.
                `false` in any other case.

        \~russian
            \brief
                Обобщённая проверка на раннее завершение

            \tparam K
                Тип ядра свёртки, которое требуется проверить на раннее завершение.
            \tparam S
                Тип состояния, соответствующего ядру свёртки `kernel`.

            \returns
                `true` в том, и только в том случае, если `K` обладает свойством раннего завершения
                над состоянием `S`, и при этом состояние `state` является финальным. Во всех
                остальных случаях возвращается `false`.

        \~  \see is_finalizable
            \see is_final
     */
    template <typename K, typename S>
    constexpr auto is_finalizable_and_final (const K & kernel, const S & state)
        -> std::enable_if_t<is_finalizable_v<K, state_symbol_t<S>>, bool>
    {
        return is_final(kernel, state);
    }

    template <typename K, typename S>
    constexpr auto is_finalizable_and_final (const K &, const S &)
        -> std::enable_if_t<not is_finalizable_v<K, state_symbol_t<S>>, bool>
    {
        return false;
    }
}
