#pragma once

#include <proxima/type_traits/detail/has_reducing_emit.hpp>
#include <proxima/type_traits/detail/has_transducing_emit.hpp>
#include <proxima/type_traits/is_reducible.hpp>
#include <proxima/type_traits/is_stateful.hpp>
#include <proxima/type_traits/is_transducible.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks whether a transducer or a reduce kernel is emitting

            \tparam T
                The transducer or the reduce kernel type.
            \tparam A
                The type of the initial symbol for the state `S`.

            \returns
                `true` if `emit` is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие завершения по требованию у преобразователя или ядра свёртки

            \tparam T
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип инициализирующего символа для состояния `S`.

            \returns
                `true`, если функция `emit` определена, и `false` в противном случае.

        \~  \see emit
            \see is_emitting_v
     */
    template <typename T, typename A, typename = void>
    struct is_emitting: std::false_type {};

    /*!
        \~english
            \brief
                Checks whether a reduce kernel is emitting

            \details
                A reduce kernel is considered emitting if the following function is defined:

                    emit: K × S -> void,

                where `K` is the reduce kernel type, `S = state_of_t<K, A>` is the state type.

            \tparam K
                The reduce kernel type.
            \tparam A
                The type of the initial symbol for the state `S`.

            \returns
                `true` if `emit` is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие завершения по требованию у ядра свёртки

            \details
                Говорят, что ядро свёртки обладает завершением по требованияю, если для него
                определена функция

                    emit: K × S -> void,

                где `K` — тип ядра свёртки, `S = state_of_t<K, A>` — тип состояния.

            \tparam K
                Тип ядра свёртки.
            \tparam A
                Тип инициализирующего символа для состояния `S`.

            \returns
                `true`, если функция `emit` определена, и `false` в противном случае.

        \~  \see emit
            \see is_reducible
     */
    template <typename K, typename A>
    struct is_emitting<K, A,
        std::enable_if_t
        <
            is_reducible_v<K, A> &&
            detail::has_reducing_emit_v<K, A>
        >>:
        std::true_type {};

    /*!
        \~english
            \brief
                Checks whether a transducer is emitting

            \details
                The transducer is considered emitting if the following function is defined:

                    emit: T × Tp × S -> void,

                where `T` is the transducer type, `Tp` is the tape type, `S = state_of_t<T, A>` is
                the state type.

                Type of the tape does not matter. It matters only that the tape consumes the output
                symbol of the transducer. Therefore, the check uses an "omnivorous" tape, and the
                tape is not exposed in the arguments of the metafunction.

            \tparam T
                The transducer type.
            \tparam A
                The type of the initial symbol for the state `S`.

            \returns
                `true` if `emit` is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие завершения по требованию у преобразователя

            \details
                Говорят, что преобразователь обладает завершением по требованию, если для него
                определена функция

                    emit: T × Tp × S -> void,

                где `T` — тип преобразователя, `Tp` — тип ленты, `S = state_of_t<T, A>` — тип
                состояния.

                Тип ленты значения не имеет. Важно лишь то, что она принимает выход
                преобразователя. Поэтому в проверке используется "всеядная" лента, и в аргументы
                метафункции лента не выставляется.

            \tparam T
                Тип преобразователя.
            \tparam A
                Тип инициализирующего символа для состояния `S`.

            \returns
                `true`, если отображение `emit` определено, и `false` в противном случае.

        \~  \see emit
            \see is_transducible
            \see is_stateful
     */
    template <typename T, typename A>
    struct is_emitting<T, A,
        std::enable_if_t
        <
            is_transducible_v<T, A> &&
            is_stateful_v<T, A> &&
            detail::has_transducing_emit_v<T, A>
        >>:
        std::true_type {};

    /*!
        \~english
            \brief
                A simplified form to check whether a transducer or a reduce kernel is emitting

            \tparam T
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `is_emitting<T, S>::value`

        \~russian
            \brief
                Упрощённая форма проверки на наличие завершения по требованию у преобразователя или
                ядра свёртки

            \tparam T
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `is_emitting_reduce_kernel<T, S>::value`

        \~  \see emit
            \see is_emitting
     */
    template <typename T, typename A>
    constexpr auto is_emitting_v = is_emitting<T, A>::value;
} // namespace proxima
