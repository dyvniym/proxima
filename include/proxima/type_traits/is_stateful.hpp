#pragma once

#include <proxima/type.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks whether a transducer or a reduce kernel has a state

            \details
                A transducer or a reduce kernel `K` is considered stateful over the symbol `A`
                if the following function is defined:

                    initialize: K × A -> S,

                where `K` is the reduce kernel or the transducer type, `A` is the symbol type,
                `S` is the state type.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `true` if `initialize` is defined and `false` otherwise.

        \~russian
            \brief
                Проверка на наличие состояния у ядра свёртки или преобразователя

            \details
                Говорят, что ядро свёртки или преобразователь `K` имеет состояние над
                символом `A`, если для него определена функция

                    initialize: K × A -> S,

                где `K` — тип ядра свёртки или преобразователя, `A` — тип символа,
                `S` — тип состояния.

            \tparam K
                Тип ядра свёртки или преобразователя.
            \tparam A
                Тип символа.

            \returns
                `true` в случае, если отображение `initialize` определено, и `false` в противном
                случае.

        \~  \see initialize
            \see is_stateful_v
            \see is_stateless
            \see is_stateless_v
     */
    template <typename K, typename A, typename = void>
    struct is_stateful: std::false_type {};

    template <typename K, typename A>
    struct is_stateful<K, A, std::void_t<state_of_t<K, A>>>: std::true_type {};

    /*!
        \~english
            \brief
                Simplified form of statefullness checking

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `is_stateful<K, A>::value`

        \~russian
            \brief
                Упрощённая форма проверки на наличие состояния

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `is_stateful<K, A>::value`

        \~  \see initialize
            \see is_stateful
            \see is_stateless
            \see is_stateless_v
     */
    template <typename K, typename A>
    constexpr auto is_stateful_v = is_stateful<K, A>::value;
} // namespace proxima
