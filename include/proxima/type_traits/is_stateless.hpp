#pragma once

#include <proxima/type_traits/is_stateful.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Checks whether a transducer or a reduce kernel does not have a state

            \details
                Is a negation of `is_stateful` metafunction.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `true` if `initialize` is not defined and `false` otherwise.

        \~russian
            \brief
                Проверка на отсутствие состояния у преобразователя или ядра свёртки

            \details
                Является отрицанием метафункции `is_stateful`.

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `true` в случае, если отображение `initialize` не определено, и `false` в противном
                случае.

        \~  \see initialize
            \see is_stateless_v
            \see is_stateful
            \see is_stateful_v
     */
    template <typename K, typename A>
    struct is_stateless: std::negation<is_stateful<K, A>> {};

    /*!
        \~english
            \brief
                Simplified form of statelessness checking

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `is_stateless<K, A>::value`

        \~russian
            \brief
                Упрощённая форма проверки на отсутствие состояния

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `is_stateless<K, A>::value`

        \~  \see initialize
            \see is_stateless
            \see is_stateful
            \see is_stateful_v
     */
    template <typename K, typename A>
    constexpr auto is_stateless_v = is_stateless<K, A>::value;
} // namespace proxima
