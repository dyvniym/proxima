#pragma once

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Discovers the output symbol of a transducer

            \details
                Transducer reads the input symbol from the input tape and writes the output symbol
                to the output tape. Input and output symbol types may be different.

                There is no direct access to the output symbol type of a transducer, therefore, a
                special way of discovering this type is required. Such mechanism is the definition
                of a special metafunction in a transducer class:

                    T::output_symbol_t<A>

                If this metafunction is defined, then its result would be the output symbol type.
                If it is not defined, then the output symbol type would be the same as the input
                symbol type.

            \tparam T
                The transducer type.
            \tparam A
                The input symbol type.

            \returns
                `T::output_symbol_t<A>`, if defined, and `A` otherwise.

        \~russian
            \brief
                Определяет тип исходящего из преобразователя символа

            \details
                В процессе работы преобразователь считывает входящий символ со входной ленты и
                записывает исходящий символ на выходную ленту. Типы входного и выходного символа,
                вообще говоря, могут не совпадать.

                Прямого доступа к типу исходящего символа нет, поэтому необходим специальный
                механизм выяснения этого типа. Таким механизмом является определение метафункции в
                классе преобразователя:

                    T::output_symbol_t<A>

                Если данная метафункция определена, то типом исходящего символа будет её результат.
                Если не определена, то в качестве типа исходящего символа будет взят тип входящего
                символа.

            \tparam T
                Тип преобразователя.
            \tparam A
                Тип входящего символа.

            \returns
                `T::output_symbol_t<A>` в случае, если данная метафункция определена, и `A` в
                противном случае.
     */
    template <typename T, typename A, typename = void>
    struct output_symbol
    {
        using type = A;
    };

    template <typename T, typename A>
    struct output_symbol<T, A, std::void_t<typename T::template output_symbol_t<A>>>
    {
        using type = typename T::template output_symbol_t<A>;
    };

    template <typename T, typename A>
    struct output_symbol<T, A, std::enable_if_t<not std::is_same_v<T, std::remove_reference_t<T>>>>
    {
        using type = typename output_symbol<std::remove_reference_t<T>, A>::type;
    };

    /*!
        \~english
            \brief
                Simplified form of discovering the output symbol of a transducer

            \tparam T
                The transducer type.
            \tparam A
                The input symbol type.

            \returns
                `output_symbol<T, A>::type`

        \~russian
            \brief
                Упрощённая форма определения типа исходящего из преобразователя символа

            \tparam T
                Тип преобразователя.
            \tparam A
                Тип входящего символа.

            \returns
                `output_symbol<T, A>::type`

        \~  \see initialize
            \see output_symbol
     */
    template <typename T, typename A>
    using output_symbol_t = typename output_symbol<T, A>::type;
} // namespace proxima
