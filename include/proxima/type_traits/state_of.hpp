#pragma once

#include <proxima/type.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Discovers the state type of a transducer or a reduce kernel over a symbol

            \details
                A transducer or a reduce kernel `T` is considered stateful over a symbol `A` if the
                following function is defined:

                    initialize: T × A -> S,

                where `T` — a transducer or a reduce kernel type, `A` — symbol type, `S` — state
                type.

            \tparam T
                The transducer or the reduce kernel type.
            \tparam A
                Symbol type.

            \returns
                `S` if `initialize` is defined and nothing otherwise (for the favor of SFINAE).

        \~russian
            \brief
                Определяет тип состояния по преобразователю или ядру свёртки и начальному символу

            \details
                Говорят, что преобразователь или ядро свёртки `T` имеет состояние над символом `A`,
                если для него определена функция

                    initialize: T × A -> S,

                где `T` — тип преобразователя или ядра свёртки, `A` — тип символа, `S` — тип
                состояния.

            \tparam T
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `S` в случае, если отображение `initialize` определено. В противном случае выходной
                тип отсутствует (это нужно для поддержки SFINAE).

        \~  \see initialize
            \see is_stateful
            \see state_of_t
     */
    template <typename T, typename A, typename = void>
    struct state_of
    {
        // Пусто во имя SFINAE.
    };

    template <typename T, typename A>
    struct state_of<T, A, std::void_t<decltype(initialize(std::declval<const T &>(), type<A>))>>
    {
        using type = decltype(initialize(std::declval<const T &>(), type<A>));
    };

    /*!
        \~english
            \brief
                Simplified form of discovering the state type of a transducer or a reduce kernel
                over a symbol

            \tparam T
                The transducer or the reduce kernel type.
            \tparam A
                Symbol type.

            \returns
                `state_of<T, A>::type`

        \~russian
            \brief
                Упрощённая форма определения типа состояния по преобразователю или ядру свёртки и
                начальному символу

            \tparam T
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `state_of<T, A>::type`

        \~  \see initialize
            \see state_of
     */
    template <typename T, typename A>
    using state_of_t = typename state_of<T, A>::type;
} // namespace proxima
