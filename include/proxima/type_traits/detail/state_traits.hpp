#pragma once

#include <type_traits>

namespace proxima::detail
{
    template <typename State, typename = void>
    struct has_symbol_type: std::false_type {};

    template <typename State>
    struct has_symbol_type<State, std::void_t<typename State::symbol_type>>: std::true_type {};

    template <typename State>
    constexpr auto has_symbol_type_v = has_symbol_type<State>::value;

    template <typename State, typename = void>
    struct has_value_type: std::false_type {};

    template <typename State>
    struct has_value_type<State, std::void_t<typename State::value_type>>: std::true_type {};

    template <typename State>
    constexpr auto has_value_type_v = has_value_type<State>::value;

    template <typename State, typename = void>
    struct state_traits_impl
    {
        // Пусто во имя поддержки SFINAE.
    };

    template <typename State>
    struct state_traits_impl
    <
        State,
        std::enable_if_t<has_symbol_type_v<State> && has_value_type_v<State>>
    >
    {
        using symbol_type = typename State::symbol_type;
        using value_type = typename State::value_type;
    };
} // namespace proxima::detail
