#pragma once

#include <proxima/type_traits/detail/black_hole.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <type_traits>

namespace proxima::detail
{
    template <typename T, typename A, typename = void>
    struct has_transducing_emit: std::false_type {};

    template <typename T, typename A>
    struct has_transducing_emit<T, A,
        std::void_t
        <
            decltype
            (
                emit
                (
                    std::declval<const T &>(),
                    std::declval<detail::black_hole_t &>(),
                    std::declval<state_of_t<T, A> &>()
                )
            )
        >>:
        std::true_type {};

    template <typename T, typename A>
    constexpr auto has_transducing_emit_v = has_transducing_emit<T, A>::value;
} // namespace proxima::detail
