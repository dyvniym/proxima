#pragma once

#include <proxima/type_traits/detail/state_traits.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                State traits

            \details
                #### Member types

                | Member type   | Definition       |
                |:------------- |:---------------- |
                | `symbol_type` | `S::symbol_type` |
                | `value_type`  | `S::value_type`  |

                If `S` does not have both member types `symbol_type` and `value_type`, then this
                template has no members by any of those names (`state_traits` is SFINAE-friendly).

            \tparam S
                State type.

        \~russian
            \brief
                Свойства состояния

            \details
                #### Типы-члены

                | Тип           | Определение      |
                |:------------- |:---------------- |
                | `symbol_type` | `S::symbol_type` |
                | `value_type`  | `S::value_type`  |

                Если `S` не содержит оба типа-члена `symbol_type` и `value_type`, то данный шаблон
                не содержит ни одного типа-члена с каким-либо из этих имён (это даёт возможность
                поддерживать SFINAE).

            \tparam S
                Тип состояния.

        \~  \see state_symbol_t
            \see state_value_t
     */
    template <typename S>
    struct state_traits: detail::state_traits_impl<S> {};
} // namespace proxima
