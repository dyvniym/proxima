#pragma once

#include <proxima/type.hpp>
#include <proxima/type_traits/is_emitting.hpp>
#include <proxima/type_traits/is_equality_comparable_with.hpp>
#include <proxima/type_traits/is_finalizable.hpp>
#include <proxima/type_traits/iterator_value.hpp>
#include <proxima/type_traits/state_of.hpp>
#include <proxima/type_traits/state_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>

#include <iterator>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                Receives a sequence, a reduce kernel and its state.
                Passes all elements through the reduce kernel, thus changing its state.
                Returns the new state that occurred after running all the symbols through the
                kernel.

            \param [first, last)
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Принимает на вход диапазон `[first, last)`, ядро свёртки и его состояние.
                Пропускает все символы через ядро свёртки, меняя тем самым его состояние.
                Возвращает новое состояние, которое возникло после прогона всех символов через
                ядро.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Обновлённое состояние ядра. Состояние после того как все символы были пропущены
                через ядро.

        \~  \see is_finalizable
     */
    template <typename InputIterator, typename Sentinel, typename State, typename ReduceKernel>
    constexpr auto reduce (InputIterator first, Sentinel last, State s, ReduceKernel & k)
        -> std::enable_if_t<not is_finalizable_v<ReduceKernel, state_symbol_t<State>>, State>
    {
        while (first != last)
        {
            k(*first, s);
            ++first;
        }

        return s;
    }

    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                This is an overload for the reduce kernels modeling the Finalizable concept.

            \param [first, last)
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Перегрузка для ядер свёртки, моделирующих концепцию раннего завершения. Т.е. делая
                шаг свёртки, нужно ещё и проверять не перешло ли ядро в конечное состояние.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Обновлённое состояние ядра. Состояние после того как все символы были пропущены
                через ядро.

        \~  \see is_finalizable
     */
    template <typename InputIterator, typename Sentinel, typename State, typename ReduceKernel>
    constexpr auto reduce (InputIterator first, Sentinel last, State s, ReduceKernel & k)
        -> std::enable_if_t<is_finalizable_v<ReduceKernel, state_symbol_t<State>>, State>
    {
        while (first != last and not is_final(k, s))
        {
            k(*first, s);
            ++first;
        }

        return s;
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                The initial state is defined implicitly inside the function and is never returned
                to the outside.

            \param [first, last)
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Поскольку начальное состояние не задано явно, оно создаётся автоматически внутри
                функции и не выдаётся наружу.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений. Иначе, просто те результаты вычислений,
                которые копятся в состоянии.

        \~  \see is_emitting
     */
    template <typename InputIterator, typename Sentinel, typename ReduceKernel>
    constexpr auto reduce (InputIterator first, Sentinel last, ReduceKernel && k)
        ->
            std::enable_if_t
            <
                not is_emitting_v<ReduceKernel, iterator_value_t<InputIterator>>
                and is_equality_comparable_with_v<InputIterator, Sentinel>,
                state_value_t<state_of_t<ReduceKernel, iterator_value_t<InputIterator>>>
            >
    {
        using value_type = iterator_value_t<InputIterator>;
        auto s = proxima::reduce(first, last, initialize(k, type<value_type>), k);
        return std::move(value(s));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                This is an overload for the reduce kernels modeling the Emitting concept.

            \param [first, last)
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the reduce kernel state after the reduction and emitting the resulting
                state.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Перегрузка для ядер свёртки, моделирующих концепцию завершения по требованию.
                Т.е. ядер, чьё состояние нужно высвободить по окончании вычислительного процесса.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений и высвобождения итогового состояния.

        \~  \see is_emitting
     */
    template <typename InputIterator, typename Sentinel, typename ReduceKernel>
    constexpr auto reduce (InputIterator first, Sentinel last, ReduceKernel && k)
        ->
            std::enable_if_t
            <
                is_emitting_v<ReduceKernel, iterator_value_t<InputIterator>>
                and is_equality_comparable_with_v<InputIterator, Sentinel>,
                state_value_t<state_of_t<ReduceKernel, iterator_value_t<InputIterator>>>
            >
    {
        using value_type = iterator_value_t<InputIterator>;
        auto s = proxima::reduce(first, last, initialize(k, type<value_type>), k);
        emit(k, s);
        return std::move(value(s));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                A convenience range-based overload.

            \param r
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Перегрузка для удобства. Принимает диапазон и ядро — всё. Инициализация
                схемы происходит внутри функции.

            \param r
                Диапазон. Т.е. сущность у которой можно получить итераторы `begin` и `end`.
            \param k
                Ядро свёртки, (приводимое или нет, не суть), которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений.
     */
    template <typename InputRange, typename ReduceKernel>
    constexpr auto reduce (InputRange && r, ReduceKernel && k)
    {
        using std::begin;
        using std::end;
        return proxima::reduce(begin(r), end(r), std::forward<ReduceKernel>(k));
    }

    /*!
        \~english
            \brief
                Reduction of a range represented by the initializer list without specifying
                the initial state of the reduce kernel

            \details
                A convenience overload. It is equivalent to the corresponding overload with a range
                instead of the initializer list.

            \param l
                A range represented by the initializer list.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона, представленного списком инициализации, без указания
                начального состояния ядра свёртки

            \details
                Перегрузка для удобства. Эквивалентна соответствующей перегрузке с диапазоном
                вместо списка инициализации.

            \param l
                Диапазон, представленный списком инициализации.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений.
     */
    template <typename Value, typename ReduceKernel>
    constexpr auto reduce (std::initializer_list<Value> l, ReduceKernel && k)
    {
        return proxima::reduce(l.begin(), l.end(), std::forward<ReduceKernel>(k));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                A convenience range-based overload.

            \param r
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Перегрузка для работы с диапазоном. Принимает диапазон и ядро, и состояние.

            \param r
                Диапазон. Т.е. сущность у которой можно получить итераторы `begin` и `end`.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, (приводимое или нет, не суть), которое задаёт схему вычислений.

            \returns
                Состояние, в котором находится схема после вычислений.
     */
    template <typename InputRange, typename State, typename ReduceKernel>
    constexpr auto reduce (InputRange && r, State s, ReduceKernel & k)
        -> std::enable_if_t<is_reducible_v<ReduceKernel, state_symbol_t<State>>, State>
    {
        using std::begin;
        using std::end;
        return proxima::reduce(begin(r), end(r), std::move(s), k);
    }

    /*!
        \~english
            \brief
                Reduction of a range represented by the initializer list

            \details
                A convenience overload. It is equivalent to the corresponding overload with a range
                instead of the initializer list.

            \param l
                A range represented by the initializer list.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона, представленного списком инициализации

            \details
                Перегрузка для удобства. Эквивалентна соответствующей перегрузке с диапазоном
                вместо списка инициализации.

            \param l
                Диапазон, представленный списком инициализации.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Состояние, в котором находится схема после вычислений.
     */
    template <typename Value, typename State, typename ReduceKernel>
    constexpr auto reduce (std::initializer_list<Value> l, State s, ReduceKernel && k)
    {
        return proxima::reduce(l.begin(), l.end(), std::move(s), k);
    }
}
