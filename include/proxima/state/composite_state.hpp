#pragma once

#include <proxima/state/nullstate.hpp>
#include <proxima/type_traits/is_stateful.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_of.hpp>
#include <proxima/type_traits/state_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <proxima/type_traits/type_identity.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                State of composite reduce kernel

            \details
                Represents a "matryoshka" of states, with the outer part of the "matryoshka"
                corresponding to a transducer `T` (the outer part of the "matryoshka" of the
                composite reduce kernel) and the inner part of the "matryoshka" corresponding to a
                reduce kernel `K` (the inner part of the "matryoshka" of the composite reduce
                kernel).

                An empty state (`nullstate`) corresponds to the stateless transducers.

            \tparam T
                The transducer forming the outer part of the "matryoshka" of the composite reduce
                kernel.
            \tparam K
                The reduce kernel forming the inner part of the "matryoshka" of the composite
                reduce kernel.
            \tparam A
                Type of the symbol from which the state was initialized.

        \~russian
            \brief
                Состояние составного ядра свёртки

            \details
                Представляет собой "матрёшку" состояний, в которой внешняя часть "матрёшки"
                соответствует некоему преобразователю `T` (внешней части "матрёшки" составного ядра
                свёртки), а внутренняя часть "матрёшки" — некоему ядру свёртки `K` (внутренней
                части "матрёшки" составного ядра свёртки).

                Преобразователям, у которых нет состояний (то есть не определена функция
                `initialize`), соответствует пустое состояние (`nullstate`).

            \tparam T
                Преобразователь представляющий внешнюю часть "матрёшки" составного ядра свёртки.
            \tparam K
                Ядро свёртки, представляющее внутрюннюю часть "матрёшки" составного ядра свёртки.
            \tparam A
                Тип символа, из которого было проинициализировано состояние.

        \~  \see initialize
            \see nullstate
            \see composite_reduce_kernel_t
            \see state_symbol_t
     */
    template <typename T, typename K, typename A>
    struct composite_state_t
    {
        using head_type =
            typename std::conditional_t
            <
                is_stateful_v<T, A>,
                state_of<T, A>,
                type_identity<nullstate_t>
            >
            ::type;
        using tail_type = state_of_t<K, output_symbol_t<T, A>>;

        using value_type = state_value_t<tail_type>;
        using symbol_type = A;

        head_type head;
        tail_type tail;
    };

    /*!
        \~english
            \brief
                Access to value of composite reduce kernel state

            \details
                The result of each reduction is stored in the state of the reduce kernel.
                The purpose of this function is to obtain that value.

            \param state
                The state of the composite reduce kernel.

            \returns
                Value of the innermost part of the "matryoshka" (the smallest one) of the state.

        \~russian
            \brief
                Доступ к значению состояния составного ядра свёртки

            \details
                Результат каждого вычисления свёртки хранится в состоянии этого ядра свёртки.
                Данная функция нужна для того, чтобы получить это значение.

            \param state
                Состояние составного ядра свёртки.

            \returns
                Значение состояния наименьшей из "матрёшек" (то есть наиболее глубоко вложенной),
                составляющих состояние.

        \~  \see composite_state_t
     */
    template <typename T, typename K, typename A>
    constexpr auto & value (composite_state_t<T, K, A> & state)
    {
        return value(state.tail);
    }

    template <typename T, typename K, typename A>
    constexpr const auto & value (const composite_state_t<T, K, A> & state)
    {
        return value(state.tail);
    }

    template <typename T, typename K, typename A>
    constexpr auto value (composite_state_t<T, K, A> && state)
    {
        return value(std::move(state).tail);
    }
} // namespace proxima
