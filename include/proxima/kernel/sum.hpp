#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The summing reduce kernel

            \details
                Counts the sum of elements that are read from the input tape.

        \~russian
            \brief
                Суммирующее ядро свёртки

            \details
                Вычисляет сумму символов, считанных со входной ленты.
     */
    struct sum_t
    {
        /*!
            \~english
                \brief
                    Reads a symbol from the input tape and adds it to the sum

                \details
                    Performs the `operator +` on the current value of the state and the symbol from
                    the input tape. The result is stored in the state value.

                \param symbol
                    The symbol from the input tape.
                \param state
                    The current state of the reduce kernel. Contains sum of the elements so far.

            \~russian
                \brief
                    Считывает символ со входной ленты и прибавляет его к сумме

                \details
                    Выполняет вызов оператора сложения над текущим значением состояния и символом,
                    считанным со входной ленты. Результат сохраняется в значении состояния.

                \param symbol
                    Символ со входной ленты.
                \param state
                    Текущее состояние ядра свёртки. Содержит текущую сумму элементов.
         */
        template <typename A, typename S>
        constexpr void operator () (A && symbol, S & state) const
        {
            value(state) = value(state) + std::forward<A>(symbol);
        }
    };

    /*!
        \~english
            \brief
                A convenience instrument for using the summation reduce kernel

            \details
                Usage examples:

                \code{.cpp}
                auto sum = proxima::reduce(sequence, proxima::sum);
                \endcode

                \code{.cpp}
                auto sum_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::sum
                    );
                auto sum = proxima::reduce(sequence, sum_of_unique_elements);
                \endcode

            \returns
                An instance of `sum_t` reduce kernel.

        \~russian
            \brief
                Инструмент для удобного создания суммирующей свёртки

            \details
                Примеры использования:

                \code{.cpp}
                auto sum = proxima::reduce(sequence, proxima::sum);
                \endcode

                \code{.cpp}
                auto sum_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::sum
                    );
                auto sum = proxima::reduce(sequence, sum_of_unique_elements);
                \endcode

            \returns
                Экземпляр ядра свёртки `sum_t`.

        \~  \see sum_t
            \see compose
            \see reduce
     */
    constexpr auto sum = sum_t{};

    /*!
        \~english
            \brief
                Initializes the initial state of the summing reduce kernel

            \tparam Arithmetic
                The type of symbols that will be summing. Must meed the requirements of
                Arithmetic concept.

            \returns
                A simple state initialized with `Arithmetic{0}`.

        \~russian
            \brief
                Инициализация начального состояния суммирующего ядра свёртки

            \tparam Arithmetic
                Тип символов, над которыми будет производиться суммирование. Обязан быть
                арифметическим.

            \returns
                Простое состояние, инициализированное значением `Arithmetic{0}`.

        \~  \see simple_state_t
            \see type_t
            \see sum_t
     */
    template <typename Arithmetic>
    constexpr auto initialize (const sum_t &, type_t<Arithmetic>)
    {
        static_assert(std::is_arithmetic_v<Arithmetic>);
        return simple_state_t<Arithmetic>{Arithmetic{0}};
    }
}
