#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <cstddef>
#include <utility>
#include <vector>

namespace proxima
{
    /*!
        \~english
            \brief
                A tag object to reserve memory for `std::vector`.

            \details
                Contains a number describing amount of objects to reserve memory using
                `std::vector::reserve`.

        \~russian
            \brief
                Объект-метка для резервирования памяти, необходимой классу `std::vector`.

            \details
                Содержит число, описывающее количество объектов, под которые следует заранее
                выделить память с помощью функции `std::vector::reserve`.

        \~  \see reserve
            \see to_vector_t
            \see to_vector
     */
    struct reserve_t
    {
        std::size_t n = 0;
    };

    /*!
        \~english
            \brief
                The reduce kernel which saves the reduction result to a container

            \details
                The container is `std::vector`.

        \~russian
            \brief
                Ядро свёртки, сохраняющее результат свёртки в массив

            \details
                В качестве массива выступает `std::vector`.

        \~  \see to_vector
     */
    struct to_vector_t
    {
        /*!
            \~english
                \brief
                    Reads a symbol from the tape and pushes it to the container

                \tparam A
                    The type of a symbol from the input tape.
                \tparam S
                    The state type. Contains `std::vector<A>`.

            \~russian
                \brief
                    Считывает с ленты символ и дописывает его в конец массива

                \tparam A
                    Тип символа, поступающего с ленты.
                \tparam S
                    Тип состояния. Содержит `std::vector<A>`.
         */
        template <typename A, typename S>
        constexpr void operator () (A && symbol, S & state) const
        {
            value(state).push_back(std::forward<A>(symbol));
        }

        /*!
            \~english
                \brief
                    Creates a reduce kernel with custom reserve

                \param r
                    The tag object defining the amount of memory to be reserved in the container
                    during the state initialization.

                \returns
                    An instance of `to_vector_t` with custom reserve.

            \~russian
                \brief
                    Создать ядро свёртки с пользовательским резервированием

                \param r
                    Объект-метка, определяющий количество памяти, которое будет зарезервировано
                    в массиве при инициализации состояния.

                \returns
                    Экземпляр ядра свёртки `to_vector_t` с заданным резервированием.

            \~  \see reserve_t
                \see reserve
         */
        constexpr to_vector_t operator () (reserve_t r) const
        {
            return to_vector_t{r};
        }

        reserve_t reserve;
    };

    /*!
        \~english
            \brief
                The initial state creation

            \tparam A
                The type of symbol, which will be read from the tape.

            \returns
                The state containing the empty container `std::vector<A>` with reserved memory for
                `t.reserve.n` elements.

        \~russian
            \brief
                Инициализация начального состояния

            \tparam A
                Тип символа, который будет считываться с ленты на каждом шаге свёртки.

            \returns
                Состояние, содержащее пустой контейнер `std::vector<A>`, в котором зарезервирована
                память под `t.reserve.n` элементов.

        \~  \see to_vector_t
            \see simple_state_t
            \see type_t
     */
    template <typename A>
    constexpr auto initialize (const to_vector_t & t, type_t<A>)
    {
        auto v = std::vector<A>{};
        v.reserve(t.reserve.n);
        return simple_state_t<std::vector<A>, A>{std::move(v)};
    }

    /*!
        \~english
            \brief
                A convenience instrument to use in `compose` function

            \details
                Usage example:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                An instance of `to_vector_t` reduce kernel.

        \~russian
            \brief
                Инструмент для использования в функции `compose`

            \details
                Пример использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                Экземпляр ядра свёртки `to_vector_t`.

        \~  \see to_vector_t
            \see compose
     */
    constexpr auto to_vector = to_vector_t{};

    /*!
        \~english
            \brief
                A convenience instrument to reserve memory for `std::vector`

            \details
                Usage example:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector(proxima::reserve(100))
                    );
                \endcode

            \returns
                An instance of `reserve_t`.

        \~russian
            \brief
                Инструмент для резервирования памяти, необходимой классу `std::vector`

            \details
                Пример использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector(proxima::reserve(100))
                    );
                \endcode

            \returns
                Экземпляр объекта-метки `reserve_t`.

        \~  \see reserve_t
            \see to_vector
            \see compose
     */
    template <typename Integer>
    reserve_t reserve (Integer n)
    {
        return reserve_t{static_cast<std::size_t>(n)};
    }
}
