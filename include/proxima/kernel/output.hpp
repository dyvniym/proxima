#pragma once

#include <proxima/kernel/fold.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Creates a reduce kernel for writing to the iterator

            \param out
                The iterator that will be contained in the state of the reduce kernel. This iterator
                will be used for the output.

            \details
                Each call to this reduce kernel writes to the iterator `out` the symbol from the
                input tape, and moves the iterator one position forward.

                Usage example:

                \code{.cpp}
                std::vector<int> v;
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::out(std::back_inserter(v))
                    );
                \endcode

            \returns
                A reduce kernel that writes to the iterator.

        \~russian
            \brief
                Создаёт ядро свёртки, записывающее в итератор

            \param out
                Итератор, который будет содержаться в состоянии ядра свёртки, и в который будет
                производиться вывод.

            \details
                Каждый вызов этого ядра производит запись в итератор `out` символа, считанного со
                входной ленты, а сам итератор продвигается на одну позицию вперёд.

                Пример использования:

                \code{.cpp}
                std::vector<int> v;
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::out(std::back_inserter(v))
                    );
                \endcode

            \returns
                Ядро свёртки, записывающее в итератор.

        \~  \see fold
            \see compose
     */
    template <typename OutputIterator>
    constexpr auto output (OutputIterator out)
    {
        return
            fold(out,
                [] (auto out, auto && value)
                {
                    *out = std::forward<decltype(value)>(value);
                    return ++out;
                });
    }
} // namespace proxima
