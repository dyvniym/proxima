#pragma once

#include <condition_variable>
#include <deque>
#include <future>
#include <mutex>

namespace proxima::detail
{
    /*!
        \~english
            \brief
                The synchronization tools of a thread pool

            \details
                Contains a queue through which tasks are passed, mutex, condition variable, and
                the task queue status indicator.

        \~russian
            \brief
                Инструменты для синхронизации пула потоков

            \details
                Содержит очередь, через которую передаются задачи, мьютекс, переменную состояния,
                а также индикатор состояния очереди задач.

        \~  \see thread_pool
            \see thread_pool_state_t
     */
    struct thread_pool_synchronization_t
    {
        std::deque<std::packaged_task<void ()>> tasks;
        std::mutex queue_mutex;
        std::condition_variable queue_cond;
        // Теоретически индикатор состояния можно было бы сделать атомарным, но в данной ситуации
        // это не оправдано, потому что очередь в любом случае управляется мьютексом, а после
        // захвата мьютекса, чтобы определить состояние очереди, индикатор нужно в любом случае
        // проверять снова.
        bool queue_is_closed = false;
    };
} // namespace proxima::detail
